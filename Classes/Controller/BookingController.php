<?php
namespace Parousia\Bookingpar\Controller;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

/***************************************************************
*  Copyright notice
*
*  (c) 2008-2013 Joachim Ruhs <postmaster@joachim-ruhs.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Plugin 'bookingpar' for the 'bookingpar' extension.
 *
 * @author        Karel Kuijpers <karelkuijpers@gmail.com>
 */


class BookingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{

	var $conf = array();
//	var $frontendUser;
	var $frontendController;
	var $sitelanguage='';

	    
    /**
     * Initialize 
     */
    public function initializeAction(): void
    {
/*		$this->frontendUser = $this->request->getAttribute('frontend.user');
		$this->pageId = $pageArguments->getPageId();
		$this->userid=$this->frontendUser->user['person_id'];
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchlogin');	
		$this->pid=$extensionConfiguration['storagePid'];
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('bookingpar');
		$this->pageuidperson = $extensionConfiguration['pagenbrsingleperson'];
		$this->defaultusergroup= $extensionConfiguration['defaultusergroup'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction required permission string: '.$extensionConfiguration['requiredpermissions']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 			
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction required permission array: '.urldecode(http_build_query(explode(',',$extensionConfiguration['requiredpermissions']),NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 			
		$this->permissions=explode(',',$extensionConfiguration['requiredpermissions']);
		sort($this->permissions,SORT_NATURAL | SORT_FLAG_CASE);
		*/
		$this->frontendController = $this->request->getAttribute('frontend.controller');
		$this->conf['lang'] = $this->frontendController->config['config']['language'];
		$entitysitelanguage=$this->request->getAttribute('language');
		$this->sitelanguage=serialize($entitysitelanguage);
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction sitelanguage: '.$this->sitelanguage."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 			
		$this->conf['pid_list'] = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK)['persistence']['storagePid'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingController getParams pidlist: '.$pidList."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingController getParams lang: '.$this->frontendController->lang."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');
		$this->conf['limitPreviewToDays'] = $this->settings['limitPreviewToDays'];
		$this->conf['limitBookingToDays']= $this->settings['limitBookingToDays'];
		$this->conf['offDutyTimeBegin']= $this->settings['offDutyTimeBegin'];
		$this->conf['offDutyTimeEnd'] = $this->settings['offDutyTimeEnd'];
		$this->conf['offDutyTimeBegin2']= $this->settings['offDutyTimeBegin2'];
		$this->conf['offDutyTimeEnd2']= $this->settings['offDutyTimeEnd2'];
		$this->conf['mylang'] = $this->conf['lang'].'-'.strtoupper($this->conf['lang']);
		$datemax=new \DateTime();
		if (!empty($this->conf['limitBookingToDays'])) 
			$datemax->add(new \DateInterval('P'.intval($this->conf['limitBookingToDays']).'D'));
		else
			$datemax->add(new \DateInterval('P10Y')); // default +10 year
		$this->conf['datemax']=$datemax->format('Y-m-d');;
		//$this->conf['settings']= $this->settings;
		$this->conf['extPath'] = PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('bookingpar'));
		$this->conf['templateFile'] = "EXT:bookingpar/Resources/Private/Templates/Booking/template.html";
		$this->conf['imagePath'] = "EXT:bookingpar/Resources/Public/Images/";
		$this->conf['sitelanguage'] = $this->sitelanguage;

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction conf: '.urldecode(http_build_query($this->conf,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 			
	}    

	
		/**
		   * action viewObjects
		   * 
		   * @return void
		   */

		public Function viewObjectsAction(): ResponseInterface {
			$row=array();
			$this->conf['displayMode']="viewObjects";

			$dt=new \DateTime();
//			$this->conf['labels'] = $label;
//			$data=array();
//			$data[ 'weekdays']=[0,0,0,1,0,0,0];


	        $assignedValues = [
				'datecur' => $dt->format('Y-m-d'),
				'datecur2' => $dt,
				'curmonth' => $dt->format('M'),
				'conf' => $this->conf,
				'datenext' => new \DateTime('2020-08-01'),
				'datemax' => new \DateTime($this->conf['datemax']),
	        ];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 			

        	$this->view->assignMultiple($assignedValues);                
			$this->eIDScriptGen();
			return $this->htmlResponse();

		}

		/**
		   * action viewAgenda
		   * 
		   * @return void
		   */

		public Function viewAgendaAction(): ResponseInterface {
			$row=array();
			$this->conf["displayMode"]='viewAgenda';

//			list($day, $month, $year) = explode('-', date('d-m-Y', time())); 
 			$dt=new \DateTime();
	        $assignedValues = [
				'datecur' => $dt->format('Y-m-d'),
				'conf' => $this->conf,
				'curmonth' => $dt->format('M'),
	        ]; 
        	$this->view->assignMultiple($assignedValues); 
			$this->eIDScriptGen();
			return $this->htmlResponse();
		}
		
		/**
		   * action showhierarchy
		   * 
		   * @return void
		   */

		public Function viewMonthAction(): ResponseInterface {
			$row=array();
			$this->conf['displayMode']="viewMonth";
			// get objects:
			$result=$this->bookingRepository->findObjects($this->conf['pid_list']);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingController viewMonthAction objects : '.http_build_query($result,'',', ').'; objects: '.$result[0]['objects']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.log');

			$dt=new \DateTime();
	        $assignedValues = [
				'objects'=> $result[0]['objects'],
				'datecur' => $dt->format('Y-m-d'),
	        ];
        	$this->view->assignMultiple($assignedValues);                
			$this->eIDScriptGen();
			return $this->htmlResponse();
		}
		

		function eIDScriptGen() {
			$objconf=array("HouresRsunday"=>"","HouresR"=>"");
			$nu= new \DateTime();
			$this->conf['extPath'] = PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('bookingpar'));
			$assetCollector = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\AssetCollector::class);
			$assetCollector->addStyleSheet('layout_1','EXT:bookingpar/Resources/Public/Css/layout.css',[],['priority' => true]);
			$assetCollector->addJavaScript('multiselect', 'EXT:parousiazoetermeer/Resources/Public/JavaScript/multiselect.js',[], ['priority' => true]);
			
			$assetCollector->addInlineJavaScript('bookingparjs',
			'	$ = jQuery.noConflict();
				/*<![CDATA[*/

			var  personaction="index.php?eID=selectperson";
			var myconfig="'.urlencode(json_encode($this->conf)).'";
			var objconfig="'.urlencode(json_encode($objconf)).'";
			var objectsselected ="";
			var datecurrent="'.$nu->format('Y-m-d').'";
			var textpic= document.getElementsByClassName("ce-textpic");
			function isDate (x){  return (null != x) && !isNaN(x) && ("undefined" !== typeof x.getDate); }
			
			function tx_booking_submit(datecur, action,event, prevevent,memo,bookinguid) {	
				var mode = action;
				if (action)
				{
						var d = new Date(datecur);
						if (datecur) datecurrent= datecur; 
						//alert("huidige datum:"+datecurrent);
						if (mode=="viewObjects") objectsselected ="";
						$.ajax({
						method: "POST",
						url: "index.php?eID=tx_bookingpar_eID",
						data: {
							"config": myconfig,
							"objectsselected": objectsselected,
							"datecur": datecurrent,
	                        "action": action,
							"prevevent": prevevent,
							"event": event,
							"memo": memo,
							"bookinguid": bookinguid
						},
						success: function(result){
							$("#eventeditview").hide();
							$("#bookingeIDResult").html(result);
							if (mode=="viewObjects") multiSelect("resobjects");
						}
					})
				}
			}

				function tx_booking_selectObject(datecur) {
				    // get selected object ids:
					var selection = $("#resobjects").val();
					if (selection) 
					{
						objectsselected=selection.toString();
						//alert("tx_booking_selectObject objectsselected:"+objectsselected);
						tx_booking_submit(datecur, \'viewMonth\',\'\',\'\',\'\',\'\');
					}
					else $("#errorBox").text("Selecteer een zaal!");
				}

				function tx_booking_viewBookingForm(datecur) {
					if(textpic[0])textpic[0].style.display = "none";			
					//$("#bookingeIDWorking").html(\'<img class="working" src="typo3conf/ext/bookingpar/Resources/Public/Images/working.gif"/>\');
				  	//alert ("bookingform: "+datecur);
					tx_booking_submit(datecur,  \'viewBookingForm\');
				}

				function tx_booking_viewAgenda(datecur) {
					//$("#bookingeIDWorking").html(\'<img class="working" src="typo3conf/ext/bookingpar/Resources/Public/Images/working.gif"/>\');
					tx_booking_submit(datecur, \'viewAgenda\');
				}

				function tx_booking_viewDayAgenda(datecur) {
					//$("#bookingeIDWorking").html(\'<img class="working" src="typo3conf/ext/bookingpar/Resources/Public/Images/working.gif"/>\');
					tx_booking_submit(datecur, \'viewDayAgenda\');
				}

				function tx_booking_book(datecur,feUserUid, id) {
					//alert ("tx_booking_book datecur:"+datecur);
					var memo = $("#tx_bookingpar_memo" + id).val();
					var memoprev = $("#tx_bookingpar_memo" + (id-1)).val();
					var prevevent = "";
					var prevbooker = "";
					if (id>0) {
						prevbooker = $("#tx_bookingpar_organiser" + (id-1)).text();
						prevevent = $("#tx_bookingpar_event" + (id-1)).text();
					}
					//alert("id:"+id+"; memoprev:"+memoprev+"; prevevent:"+prevevent+"; prevbooker:"+prevbooker);
					if (memo.length < 2 && id>0 && prevbooker==feUserUid) {memo = memoprev;}
					if (memo.length < 2)
					{
							$("#errorBox").text("Doel invullen!");;
							return false;
					}
					if (prevbooker!=feUserUid || memoprev!=memo) prevevent="";
					tx_booking_submit(datecur, "bookObject","",prevevent,memo);
				}

				function tx_booking_delete(bookinguid,eventuid) {
					tx_booking_submit("","delete",eventuid,"","",bookinguid);
				}
				
				function tx_booking_showedit(event) {
				$.ajax({
					method: "POST",
					url: "index.php?eID=tx_bookingpar_eID",
					data: {
						"config": myconfig,
						"objectsselected": objectsselected,
						"datecur": datecurrent,
                        "action": "editEvent",
						"event": event,
					},
					success: function(result){
						if (result)
						{
							//alert("result"+result);
							$("#eventeditview").html(result);
							$("#eventeditview").show();
							$("#errorBoxevent").hide();
	                        $("html,body").animate({
                            scrollTop: $("#eventeditview").offset().top - 80
                        });

							multiSelect("resobjectsEdit");
						  	if ($("#repeatcheck").prop("checked")) {
								$(".repeatevent").show();
								if ($("#repeatperiod").val()=="W"){$(".repeatweek").show();$(".repeatmonth").hide();}else{$(".repeatmonth").show();$(".repeatweek").hide();}
							}	
							else $(".repeatevent").hide();
//							if (beheer) {
//								$("#persoonorganiser").prop("disabled", false);
//							}
						}
					}
				});
				}
				
				function tx_booking_editsubmit(event, action) {

				//var mode = action;
				var selection = $("#resobjectsEdit").val();
				var startdate = $("#event_startdate").val();
				var enddate = $("#event_enddate").val();
				var repeatperiod="";
				var repeatinterval=0;
				//alert("organiserid:"+$("#idorganiser").val());
				wkd="";
				msq="";
				if (action=="cancelEvent"){
					$("#eventeditview").hide();
					return;
				}
				if (action !="deleteEvent"){
			    	// get selected object ids:
					if (!selection) {
						$("#errorBoxevent").text("Kies een zaal!");
						$("#errorBoxevent").show();
						return;
					}
/*					if (!$("#event_starthour").spinner("isValid")){
						$("#errorBoxevent").text("Ongeldig startuur: "+$("#event_starthour").val());
						$("#errorBoxevent").show();
						return;
					}
					if (!$("#event_endhour").spinner("isValid")){
						$("#errorBoxevent").text("Ongeldig einduur: "+$("#event_endhour").val());
						$("#errorBoxevent").show();
						return;
					}
					if (!$("#repeatinterval").spinner("isValid")){
						$("#errorBoxevent").text("Ongeldig aantal \"Elke\": "+$("#repeatinterval").val());
						$("#errorBoxevent").show();
						return;
					} */
					if ($("#repeatcheck").prop("checked")) {
						repeatperiod=$("#repeatperiod").val();
						repeatinterval=$("#repeatinterval").val();
						if (repeatperiod=="W"){
							if ($("#wk1").prop("checked")) wkd+="1"; 
							if ($("#wk2").prop("checked")) wkd+="2";
							if ($("#wk3").prop("checked")) wkd+="3";
							if ($("#wk4").prop("checked")) wkd+="4";
							if ($("#wk5").prop("checked")) wkd+="5";
							if ($("#wk6").prop("checked")) wkd+="6";
							if ($("#wk7").prop("checked")) wkd+="7";
						}
						else {
							if ($("#ms1").prop("checked")) msq+="1";
							if ($("#ms2").prop("checked")) msq+="2";
							if ($("#ms3").prop("checked")) msq+="3";
							if ($("#ms4").prop("checked")) msq+="4";
							wkd=$("#weekday").val();
						}
					}
				}
				$.ajax({
					method: "POST",
					url: "index.php?eID=tx_bookingpar_eID",
					data: {
						"config": myconfig,
						"objectsselected": objectsselected,
						"datecur": datecurrent,
                        "action": action,
						"event": event,
						"memo": $("#event_memo").val(),
						"organiser": $("#persoonorganiser").val(),
						"idorganiser": $("#idorganiser").val(),
						"startdate": startdate.toString(),
						"starthour": $("#event_starthour").val(),
						"endhour": $("#event_endhour").val(),
						"objectsselectednw": selection.toString(),
						"repeatperiod": repeatperiod.toString(),
						"repeatinterval": repeatinterval,
						"weekdays": wkd,
						"monthsequencenbr": msq,
						"enddate": enddate
					},
					success: function(result,status,xhr){
						if(xhr.getResponseHeader("SAVE_SUCCESS") == 1){
							$("#eventeditview").hide();
							$("#bookingeIDResult").html(result);
						}
						else if(xhr.getResponseHeader("SAVE_SUCCESS") == 2) $("#eventeditview").hide(); // cancel
						else {
							$("#errorBoxevent").html(result);
							$("#errorBoxevent").show();
						}
					}
				})
				}

				$.fn.center = function () {
				    this.css("position","fixed");
				    this.css("top", (($(window).height() - this.outerHeight()) / 2) /*+ $(window).scrollTop()*/ + "px");
				    this.css("left", (($(window).width() - this.outerWidth()) / 2) /*+ $(window).scrollLeft()*/ + "px");
				    return this;
				  }
			',[],['priority' => true]);
			
			
/*				 $( function() {
				    $( ".draggable" ).draggable();
				  } );
				</script>',[],['priority' => true]); */

		}

}
