<?php
namespace Parousia\Bookingpar\Hooks;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Lang\LanguageService;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Core\Environment;
//use TYPO3\CMS\Fluid\View\StandaloneView;
use Parousia\Churchpersreg\Hooks\FluidTemplate;

//use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Parousia\Bookingpar\Domain\Model\Booking;
use Parousia\Bookingpar\Domain\Model\Day;
use Parousia\Bookingpar\Domain\Model\BookingHour;
use Parousia\Bookingpar\Domain\Model\CalendarDay;

use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;

ini_set("display_errors",1);
ini_set("log_errors",1);
mb_internal_encoding("UTF-8");

/***************************************************************
*  Copyright notice
*
*  (c) 2009-2013 Joachim Ruhs (postmaster@joachim-ruhs.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
* class.tx_bookingpar_eID.php
*
*
* @author karel kuijpers <karelkuijpers@gmail.com>
*/

/**
 *
 *
 * @author Karel Kuijpers <karelkuijpers@gmail.com>
 * @package TYPO3
 * @subpackage tx_bookingpar
 */

class tx_bookingpar_eID {
	protected $ref='';
	protected $pid='';
	protected $conf='';
	protected $datecur;
	protected $objectuids='';
	protected $feuserid;
	protected $personid;
	protected $templateService;
	protected $db;
	protected $answer='';
	protected $beheer=0;
	protected $permissie;
	protected $datemax;
	protected $antwoord='';
	protected $aParms;
	protected $languageServiceFactory;
	protected $languageService;
	protected $sitelanguage;
	public $request;
	


/**
 * @param ServerRequestInterface $request
 * @param ResponseInterface $response
 * @return ResponseInterfacehttps://mail.google.com/mail/u/0?ui=2&ik=416e5dc449&attid=0.1&permmsgid=msg-f:1639766971097855127&th=16c19f5f52b4b497&view=fimg&disp=thd&attbid=ANGjdJ-pt-6MMAcUvg5OgDTLUK97HwAqNTlG18cNEz9xbReclRHJfbMnPTUP_xkLQSWA5Y4HaNFj7tAtHi18BIY3OxLI5zyvMsSoKaytnMUkz5kMXBdrA4b44BFn-yQ&ats=2524608000000&sz=w1919-h926
 */
	public function processRequest(ServerRequestInterface $request):ResponseInterface
	{
		session_start();
		$response = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Http\Response::class);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar start'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!empty($_SESSION["permissie"])){
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar session permissie : '.http_build_query($_SESSION,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->personid = $_SESSION['userid'];
			$this->feuserid = $_SESSION['feuserid'];
			$this->permissie=$_SESSION["permissie"];
			$this->beheer = ($this->Heeftpermissie("Reserveringenbeheer")?'1':'0');
		}
		else 
		{
			$response->getBody()->write("Sessie verlopen. Ververs het scherm.");
			return $response;
		}
   		// Get/Post Variables
		//$this->aParms = GeneralUtility::_GP('tx_bookingpar');
		$this->request=$request;
		$this->aParms=$request->getParsedBody(); 
//		if (isset($this->aParms))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar processrequest aParms : '.urldecode(http_build_query($this->aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->aParms['datecur']))$this->datecur = new \Datetime($this->aParms['datecur']);else $this->datecur = new \Datetime();

		if (isset($this->aParms['action']))$action=$this->aParms['action'];else $action='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar processrequest action : '.$action."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		// reading data vars for $this->conf
		if (isset($this->aParms['config']))$config = urldecode($this->aParms['config']);else $config='';
		$this->conf = json_decode($config,true);
		//if (is_array($this->conf))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initialize conf : '.urldecode(http_build_query($this->conf,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->aParms['objectsselected']))$this->objectuids=$this->aParms['objectsselected'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar processrequest objectsselected: '.$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		if(isset($this->conf['datemax']))$this->datemax=new \DateTime($this->conf['datemax']);
		// Init language
		$lang='';
		if (isset($this->conf['lang']))$lang=$this->conf['lang'];
		setlocale(LC_TIME,$lang.'_'.strtoupper ($lang) );
		if (isset($this->conf['sitelanguage']))
		{
			$sLanguage=$this->conf['sitelanguage'];
			$this->sitelanguage=unserialize($sLanguage);	
			$this->languageServiceFactory = GeneralUtility::makeInstance(LanguageServiceFactory::class);
			$this->languageService=$this->languageServiceFactory->createFromSiteLanguage($this->sitelanguage);		
	//		$trans=$this->languageService->sL('LLL:EXT:bookingpar/Resources/Private/Language/locallang.xlf:occupied'); 
		}
		churchpersreg_div::connectdb($this->db);
		$this->antwoord ='';
		switch ($action) { 
			case 'viewObjects': $this->viewObjects();
			break;
		    case 'viewMonth': $this->viewMonth();
		    break;
		    case 'viewBookingForm': $this->viewBookingForm();
		    break;
		    case 'bookObject': $this->bookObject();
		    break;
		    case 'participate': $this->participate();
		    break;
		    case 'delete': $this->deleteBooking();
		    break;
		    case 'viewAgenda': $this->viewMonth();
		    break;
			case 'editEvent': $this->vieweditBooking();
			break;
			case 'saveEvent': $this->saveEvent();
			break;
			case 'cancelEvent': $this->cancelEvent();
			break;
			case 'deleteEvent': $this->deleteEvent();
			break;
		    default: die('Bad mode!');
//		    default: $this->antwoord = 'Bad mode!';
		} 
		$response->getBody()->write($this->antwoord);
		return $response;
	}

	function getObjects($uid = '') {
		//if (is_array($this->conf))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getObjects uid:'.$uid.', conf : '.urldecode(http_build_query($this->conf,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
   		$fields = "uid,name,number,hours,sundayhours,multibooking";
		if (isset($this->conf['pid_list']))$pid_list=$this->conf['pid_list'];else $pid_list='';
		$where = "  pid IN (".$pid_list.") AND deleted='0' AND hidden='0'";
		if (!empty($uid)){
			$where .= " AND uid in ( " . $uid. " )";
		}
		//$this->aParms['orderBy'] = 'number';
		//$this->aParms['sort'] = '';
		$query='select '.$fields.' from tx_bookingpar_object where '.$where.' order by number ';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar getobjects query:'.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$result = $this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar getobjects error : '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//				$result=$result->fetch_all(MYSQLI_ASSOC);
//		$objects=$result->fetch_all(MYSQLI_ASSOC);

		$data = array();
		$i = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)){
//			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getObjects: '.$row['name']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');			
			$data['uid'][$i]		=	$row['uid'];
			$data['name'][$i]		=	$row['number'].' '.$row['name'];
			$data['hours'][$i]		=	$row['hours'];
			$data['sundayhours'][$i]		=	$row['sundayhours'];
			$data['multibooking'][$i]		=	$row['multibooking'];
			$i++;
		}
		return $data; 
//		return $objects;

	}

	 /**
     * Method getSelectObjects 
	 *
	 * Returns the objects
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
	function getSelectObjects() {
   		$fields = "uid,concat(number,' ',name) as name";
		$where = "  pid IN (".$this->conf['pid_list'].") AND deleted='0' AND hidden='0' and instr(concat(' ,',monthsblocked,','),concat(',',MONTH(CURDATE()),',')) < 1";
		$this->aParms['orderBy'] = 'number';
		$this->aParms['sort'] = '';
		$query='select '.$fields.' from tx_bookingpar_object a where '.$where.' order by '.$this->aParms['orderBy'] . ' ' . $this->aParms['sort'] ;
		$result = $this->db->query('select '.$fields.' from tx_bookingpar_object a where '.$where.' order by '.$this->aParms['orderBy'] . ' ' . $this->aParms['sort'] );
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingpar getSelectObjects error : '.$this->db->error.'; num:'.$result->num_rows."; query:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        $objects = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		
		while ($row = $result->fetch_array(MYSQLI_ASSOC)){
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingparEID result row: '.$row['name'].'; uid:'.$row['uid']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$object=new Booking();
			$object->setName($row['name']);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingparEID object naam: '.$object->getName()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$object->setUid(intval($row['uid']));
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookingparEID object uid: '.$object->getUid()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$objects->attach($object);
		}
		return $objects;

	}

	function getBookingsOfDay() {
		$datestr=$this->datecur->format('Y-m-d');
		$dt=new \DateTime($datestr);
		$day=$dt->getTimeStamp();
   		$fields = "a.uid,a.feuseruid,a.startdate,a.objectuid,a.memo,a.eventuid,e.feuseruid as organiser,concat(o.number,' ',o.name) as objectname,o.number,o.name,o.multibooking,o.maxparticipants,roepnaam,concat_ws(' ',tussenvoegsel,achternaam) as achternaam, concat_ws(' ',roepnaam,tussenvoegsel,achternaam) as bookername ";
		$where = "  a.pid IN (".$this->conf['pid_list'].") AND a.deleted='0' AND a.hidden='0' AND b.deleted='0' AND b.disable='0'";	
		if (!empty($this->objectuids))
			$where .= "  AND objectuid in (" . $this->objectuids . ")";
		$where .= " AND a.startdate >= " . $day . " AND a.startdate < " .
			($day + 3600 * 24);
		$where .= " AND b.uid = a.feuseruid AND a.objectuid = o.uid AND p.uid=b.person_id AND e.uid=a.eventuid";
		$this->aParms['orderBy'] = 'a.startdate,o.number,a.sorting,bookername';
		$this->aParms['sort'] = 'asc';
		$query='select '.$fields.' from tx_bookingpar a, fe_users b, tx_bookingpar_object o, persoon p,tx_bookingpar_event e where '.$where.' order by '.$this->aParms['orderBy'] . ' ' . $this->aParms['sort'];
		$result = $this->db->query($query);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getBookingsOfDay error: '.$this->db->error.'; query : '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$data = array();
		$i = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)){
			$data['uid'][$i]			= $row['uid'];
			$data['startdate'][$i]		= $row['startdate'];
			$data['objectuid'][$i]		= $row['objectuid'];
			$data['eventuid'][$i]		= $row['eventuid'];
			$data['memo'][$i] 			= stripslashes($row['memo']);
			$data['feUserName'][$i]     = $row['bookername'];
			$data['roepnaam'][$i]     	= $row['roepnaam'];
			$data['achternaam'][$i]     = $row['achternaam'];
			$data['feUserUid'][$i]     	= $row['feuseruid'];
			$data['organiser'][$i]     	= $row['organiser'];
			$data['objectname'][$i]		= $row['objectname'];
			$data['nameobject'][$i]		= $row['name'];
			$data['number'][$i]			= $row['number'];
			$data['multibooking'][$i]	= $row['multibooking'];
			$data['maxparticipants'][$i]	= $row['maxparticipants'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getBookingsOfDay data ['.$i.'] startdate: '.$data['startdate'][$i].'; memo : '.$data['memo'][$i]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$i++;
		}
		//if (is_array($data))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getBookingsOfDay data: '.urldecode(http_build_query($data,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		return $data;

	}
	
	function getBookingsCounts() {
/*		$lengthOfMonth = array (1 => 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        // leap year calculating....
        if ( date("L", mktime(0,0,0,1,1,$this->aParms['year'])) == 1) {
            $lengthOfMonth[2] = 29;
		} */

   		$fields = "count(*) as counts";
		$interval=new \DateInterval('P1D');
		$nd=new \DateTime($this->datecur->format('Y-m').'-01');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getBookingsCounts start: '.$nd->format("Y-m-d")."; lengte maand: ".$nd->format('t')."; objectuids: ".$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		for ($d = 1; $d <= $nd->format('t'); $d++) {
			$data[$d] = 0;
			$where = "  a.pid IN (".$this->conf['pid_list'].") AND a.deleted='0' AND a.hidden='0'".
			" AND startdate >= " . $nd->getTimestamp() ." AND startdate <= " . ($nd->getTimestamp() + 3600 * 23);
			if (!empty($this->objectuids)) $where .= "  AND objectuid in (" . $this->objectuids.")"; 
			$this->aParms['orderBy'] = '';
			$this->aParms['sort'] = '';
			$query='select '.$fields.' from tx_bookingpar a where '.$where;
			$result = $this->db->query($query);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getBookingsOfDay ['.$d.']-'.$nd->format('Y-m-d').' query: '.$query.'; error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$data[$d]	= $row['counts'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'count {'.$d.'] '.$nd->format("Y-m-d").': '.$data[$d]."; query:".$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$nd->add($interval);
		}
		return $data;

	}
	function getEvent($uid = 0) {
   		$fields = "e.uid,e.startdate,e.enddate,e.tstamp,e.crdate,e.memo,repeatperiod,repeatinterval,weekdays,monthsequencenbr,starthour,endhour,objectuids,e.feuseruid as organiser,concat_ws(' ',roepnaam,tussenvoegsel,achternaam) as bookername";
		$where = " b.deleted='0' AND b.disable='0' and b.uid = e.feuseruid AND p.uid=b.person_id and e.pid IN (".$this->conf['pid_list'].") AND e.deleted='0' AND e.hidden='0'";
		if ($uid != 0){
			$where .= " AND e.uid in ( " . $uid. " )";
		}
		$query='select '.$fields.' from tx_bookingpar_event e,fe_users b,persoon p where '.$where;
		$result = $this->db->query($query );
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getEvent query: '.$query.'; error: '.$this->db->error.'; aantal rijen:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$data = $result->fetch_array(MYSQLI_ASSOC);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getEvent data: '.http_build_query($data,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		return $result->fetch_array(MYSQLI_ASSOC);

	}

	function viewObjects($uid = 0) {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects uid : '.$uid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$listobjects= $this->getSelectObjects();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects listobjects'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
    	$assignedValues = [
			'objects' => $listobjects,
			'datecur' => $this->datecur->format("Y-m-d"),
			'lang' => $this->conf['lang'],
			'extensionname' => "Bookingpar",
//			'label' => $this->conf['labels'],
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects language : '.$GLOBALS['BE_USER']->uc['lang']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//		$GLOBALS['BE_USER']->uc['lang']
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	//	$out= $this->renderFluidTemplate("ViewObjectsList.html", $assignedValues) ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$this->antwoord .=   $out;
		try {
				$this->antwoord= FluidTemplate::render("Booking/ViewObjectsList.html", $assignedValues,$this,'bookingpar','churchpersreg') ;
		} catch (Exception $e) {
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects render exception : '.$e->getMessage()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		    //echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewObjects antwoord : '.$this->antwoord."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

	}

	function viewMonth() {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth objectuids : '.$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$uids=$this->objectuids;
		if (empty($uids)){
			$objects=$this->getObjects();
			//$this->objconf['objectuids']=implode(',',$objects['uid']);
		}
		else 
		{
			$objects = $this->getObjects($uids);
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'objects from getobjects : '.GeneralUtility::array2xml($objects)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		

		$objectnames='';
		$uids='';
		if (isset($objects['uid']))	$nbrobj=count($objects['uid']); else $nbrobj=0;
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'objects nbrobj : '.$nbrobj."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		//if (is_array($this->objconf["HouresRsunday"]))$nbrhrsSun=count($this->objconf["HouresRsunday"]); else $nbrhrsSun=0;
		//if (is_array($this->objconf["HouresR"]))$nbrhrsNor=count($this->objconf["HouresR"]); else $nbrhrsNor=0;

		for ($obj = 0; $obj < $nbrobj; $obj++) 
		{	$objectnames.=', '.$objects['name'][$obj];
			$uids.=','.$objects['uid'][$obj];
		}
		$uids=substr($uids,1);
		$objectnames=substr($objectnames,2);
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'objectnames : '.$objectnames."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		// determine reservable hours:
		$HouresR=$this->getReservableHours(array(2),$objects); //normal weekday
		$HouresRsunday=$this->getReservableHours(array(7),$objects); //sunday
		
		if (is_array($HouresRsunday))$nbrhrsSun=count($HouresRsunday); else $nbrhrsSun=0;
		if (is_array($HouresR))$nbrhrsNor=count($HouresR); else $nbrhrsNor=0;
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'HouresRsunday count nbrhrsSun : '.$nbrhrsSun."; nbrhrsNor ".$nbrhrsNor."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		
		// fetch the number of all bookings per day:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth before getBookingsCount uids : '.$uids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$counts = $this->getBookingsCounts(); // $this->conf['objectid']==0
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingsCounts : '.GeneralUtility::array2xml($counts)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		

		//$newconf=$this->objconf;
		$interval=new \DateInterval('P1M');
		$datestr=$this->datecur->format('Y-m');
		$datestr.='-01';
		$nd=new \DateTime($datestr);
		$dateprev=new \DateTime($datestr);
		$dateprev->sub($interval);
		$datenext= new \DateTime($datestr);
		$datenext->add($interval);
		

		// calculating the left spaces to get the layout right
		$wd = $nd->format('w');
		$wd = ($wd == 0)? 7 : $wd;
//       	for ( $s = 0; $s <  $wd ; $s++){
//       		$out .= '<td class="noDay"> </td>';
//		}
		$nowd= new \DateTime();
		$nowt=$nowd->format('Y-m-d');
		$interval=new \DateInterval('P1D');
        $calendardays = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();

		for ($d = 1; $d <=$nd->format('t'); $d++) {
			$day= new CalendarDay();
			$day->setUid($d);

			$md= $this->datecur->format('Y-m-').str_pad($d, 2, '0', STR_PAD_LEFT);
			$day->setDateday($md);

			if ($nowt == $md)
			    $classDay = 'today';
			else $classDay = '';
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth md : '.$md.'; nowt:'.$nowt.'; class:'.$classDay."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		

			$thisday=new \DateTime($md);
			$dayofweek=$thisday->format("N");
			$day->setDayofweek($dayofweek);
			$theDate = $thisday->getTimestamp();
			if ($this->conf['offDutyTimeBegin'] && $theDate >= $this->conf['offDutyTimeBegin']&& $theDate <= $this->conf['offDutyTimeEnd'] 
			|| $this->conf['offDutyTimeBegin2'] && $theDate >= $this->conf['offDutyTimeBegin2']&& $theDate <= $this->conf['offDutyTimeEnd2'])
				$classDay .= ' offDutyTime';
			if ($thisday>$this->datemax)$classDay .= ' beyondlimit';
			if ($dayofweek==7)
				$nbrhrs=$nbrhrsSun;
			else
				$nbrhrs=$nbrhrsNor;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth d : '.$d.'; nbrhrs:'.$nbrhrs.'; counts:'.$counts[$d].'; nbrobj:'.$nbrobj."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
			if  ((($nbrhrs-$counts[$d]/$nbrobj))>1) 
			{
				if ($counts[$d] > 0)
				{
					$classDay.=' someAvailable';
					$title='Enkele uren vrij';
				}
				elseif ($counts[$d] == 0) 
				{
       				if ($this->conf['offDutyTimeBegin'] && $theDate >= $this->conf['offDutyTimeBegin']&& $theDate <= $this->conf['offDutyTimeEnd']
					|| $this->conf['offDutyTimeBegin2'] && $theDate >= $this->conf['offDutyTimeBegin2']&& $theDate <= $this->conf['offDutyTimeEnd2']) 
					{
						$classDay.=' available';
						$title='Reserveren niet mogelijk';
       				} else{
						if ($thisday>$this->datemax)
						{
							$classDay.=' beyondlimit';
							$title='Buiten boekbare periode';
	       				} 
						else
							{
								$classDay.=' available';
								$title='Geen boekingen';
		       				} 
					}
				}
			}
			else {
				$classDay.=' notAvailable';
				$title='Bezet';
			}
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth md : '.$md.'; class:'.$classDay."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
			
			$day->setClassday($classDay);
			
			$day->setTitle($title);
			if ($this->conf['offDutyTimeBegin'] && $theDate >= $this->conf['offDutyTimeBegin']&& $theDate <= $this->conf['offDutyTimeEnd'] 
			|| $this->conf['offDutyTimeBegin2'] && $theDate >= $this->conf['offDutyTimeBegin2']&& $theDate <= $this->conf['offDutyTimeEnd2']
			|| $thisday>$this->datemax)
			    $day->setClickable(0);
			else $day->setClickable(1);
			$week=$thisday->format('W');
			$day->setWeek($week);
/*			$wd = $thisday->format('w');
			if ($wd == 0) {
				$week = $thisday->add($interval)->format('W');
				$day->setWeek($week);
			}*/			
			$calendardays->attach($day);
		}
	/*	$closingstring='';
		if ($dayofweek<7)
		{
			for ($i=1;$i<=(7-$dayofweek);$i++)$closingstring.='<td></td>';
		}
		$closingstring.='</tr>'; */

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'final out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		

    	$assignedValues = [
			'objectnames' => $objectnames,
			'conf' => $this->conf,
			'datecur' => $this->datecur,
			'curmonth' => $this->datecur->format('M'),
			'dateprev' => $dateprev,
			'datenext' => $datenext,
			'datemax' => new \DateTime($this->conf['datemax']),
			'calendardays' => $calendardays,
			'lang' => $this->conf['lang'],
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		//$out= $this->renderFluidTemplate("MonthView.html", $assignedValues) ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//		$this->antwoord .=   $out;
		$this->antwoord= FluidTemplate::render("Booking/MonthView.html", $assignedValues,$this,'bookingpar') ;


		//$this->antwoord .=   $this->templateService->substituteMarkerArray($template, $marks);
	}



	function viewBookingForm($message = '') {
		// get object name
		$uids=$this->objectuids;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: objectuids '.$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		$objects = $this->getObjects($uids);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm objects : '.urldecode(http_build_query($objects,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$objectnames='';
		$nbrobj=count($objects['uid']);
		for ($obj = 0; $obj < $nbrobj; $obj++) 
		{	$objectnames.=', '.$objects['name'][$obj];
		}
		$objectnames=substr($objectnames,2);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: objectnames '.$objectnames."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		


		$datestr=$this->datecur->format('Y-m-d');
		$this->datecur=new \DateTime($datestr);
		$interval=new \DateInterval('P1D');

		$nu=new \DateTime();
		if ($this->datecur < $nu) $classTODAY = 'pastDay'; else $classTODAY='';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: datecur str '.$datestr."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		$datenext=new \DateTime($datestr);
		$datenext->add($interval);
		$dateprev=new \DateTime($datestr);
		$dateprev->sub($interval);
		$data = $this->getBookingsOfDay();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: getBookingsOfDay '.GeneralUtility::array2xml($data)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
		// determine smallest set of hours:
		$aHours=array();
		$dayofweek=$this->datecur->format('N');  //7=Sunday
		$aDates = array($dayofweek);
		if ($this->conf['displayMode']=='viewObjects')$aHours = $this->getReservableHours($aDates,$objects);
		else $aHours=$this->getReservableHours(array(2),$objects); //all hours


		// filter hours on Sunday, skip hours till 12 o'clock because of service:
		$prvj = 0;
		$prvevent=0;
		
		$dt=new \DateTime($datestr);
		$day = $dt->getTimeStamp();
        $bookinghours = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();

		if (is_array($aHours))
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: aHours '.GeneralUtility::array2xml($aHours)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
			$aEventProcessed = array();
			for ($i = 0; $i < count($aHours); $i++) {
				$occupied = 0;

				$aHours[$i] = (int) $aHours[$i];
				$hourstr=$aHours[$i];

				if (isset($data['feUserName']))	
				{
					for ($j = $prvj; $j < count($data['feUserName']); $j++) {
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: startdate['.$i.']: '.$data['startdate'][$j].'; compare with day: '.($day + $aHours[$i] * 3600)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
						$bookinghour= new BookingHour();
						$bookinghour->setUid($i);
						
					    if ($data['startdate'][$j] == $day + $aHours[$i] * 3600) 
						{	
							if ($j==$prvj || $data['startdate'][$j]!=$data['startdate'][$j-1])$bookinghour->setHour($hourstr);
							$bookinghour->setObjectname($data['objectname'][$j]);
							$bookinghour->setNumber($data['number'][$j]);
							$bookinghour->setNameobject($data['nameobject'][$j]);
							$bookinghour->setUsername($data['feUserName'][$j]);
							$bookinghour->setRoepnaam($data['roepnaam'][$j]);
							$bookinghour->setAchternaam($data['achternaam'][$j]);
						  	$bookinghour->setBookable(0);
							$bookinghour->setMemo($data['memo'][$j]);
							
							$occupied = 1;
							$bookinghour->setEventuid($data['eventuid'][$j]);
							$bookinghour->setBookinguid($data['uid'][$j]);
		
							// delete only if feUserUid is fe user 
							if ($day + $aHours[$i] * 3600 > time())
							{
							//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: beheer: '.$this->beheer.'; multibooking['.$j.']: '.$data['multibooking'][$j].'; organiser: '.$data['organiser'][$j].'; data feuserid: '.$data['feUserUid'][$j]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
								$eventuid=intval($data['eventuid'][$j]);
								if (!in_array($eventuid,$aEventProcessed) && !empty($eventuid) ){
									$prvevent=$eventuid;
									$aEventProcessed[] = $eventuid;
									$bookinghour->setEditable(1);
								}
								if ($this->feuserid == $data['feUserUid'][$j] || ($this->beheer && $data['multibooking'][$j]==0)) {
//									if (!in_array($eventuid,$aEventProcessed) && !empty($eventuid) && ($data['organiser'][$j]==$this->feuserid || ($this->beheer && $data['multibooking'][$j]==0))){
									$bookinghour->setDeletable(1);
								}
								elseif ($data['multibooking'][$j]==1 && $this->feuserid != $data['organiser'][$j] && $data['feUserUid'][$j]==$data['organiser'][$j])
								{
									// check if user already partipated for this object and time:
									$result = $this->multi_array_search($data, array('startdate' => $data['startdate'][$j], 'objectuid' => $data['objectuid'][$j],'feUserUid' => $this->feuserid));
									if (empty($result))
									{	
										// check if maximum participants reached for this time and object:
										$result = $this->multi_array_search($data, array('startdate' => $data['startdate'][$j], 'objectuid' => $data['objectuid'][$j]));
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewBookingForm: maxparticipants: '.$data['maxparticipants'][$j].'; compare with hour count: '.count($result)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
										if (count($result) < $data['maxparticipants'][$j])
										{
											$startdate= new \dateTime();
											$startdate->setTimestamp($data['startdate'][$j]);
											$bookinghour->setStartdate($startdate);
											$bookinghour->setParticipatable(1);
										}
									}
								}
							}
							$bookinghours->attach($bookinghour);
						}


					}  //endfor username
				}  //endif username
				if (!$occupied){
					$bookinghour= new BookingHour();
					$bookinghour->setUid($i);
					$bookinghour->setHour($hourstr);
					if ($day + $aHours[$i] * 3600 > time()) $bookinghour->setBookable(1);
					if (($day + $aHours[$i] * 3600) > time() && $this->feuserid) {
						$startdate = new \DateTime();
						$startdate->setTimestamp($day + $aHours[$i] * 3600);
						$bookinghour->setBookable(1);
						$bookinghour->setStartdate($startdate);
					}
	
					if ($this->conf['offDutyTimeBegin'] && $day >= $this->conf['offDutyTimeBegin'] && $day <= $this->conf['offDutyTimeEnd']
					|| $this->conf['offDutyTimeBegin2'] && $day >= $this->conf['offDutyTimeBegin2']&& $day <= $this->conf['offDutyTimeEnd2'])
						$bookinghour->setBookable(0);
					if ($this->conf['limitBookingToDays'] && $day > (time() + $this->conf['limitBookingToDays'] * 86400)) $bookinghour->setBookable(0);
	
					$bookinghours->attach($bookinghour);
	
				}  //endif occupied
			} // for ($i = 0; $i < count($aHours); $i++) {
		}  // endif is_array hours 

		$assignedValues = [
			'objectnames' => $objectnames,
			'conf' => $this->conf,
			'datecur' => $this->datecur,
			'dateprev' => $dateprev,
			'datenext' => $datenext,
			'datemax' => $this->datemax,
			'feuserid' => $this->feuserid,
			'bookinghours' => $bookinghours,
			'lang' => $this->conf['lang'],
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth assignedValues : '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		//$out= $this->renderFluidTemplate("ViewBookingForm.html", $assignedValues) ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'viewMonth out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

//		$this->antwoord .=   $out;
		$this->antwoord= FluidTemplate::render("Booking/ViewBookingForm.html", $assignedValues,$this,'bookingpar') ;
		
	}
 /**
   * Multi-array search
   *
   * @param array $array (key=> {0=>value1,1=>value2,...}
   * @param array $search ({key=>value,})
   * @return array
   */
	function multi_array_search($array, $search)
	{
		// Create the result array
      	// Iterate over each search condition
    	// find keys in corresponding array element
		$result=array();
      	foreach ($search as $k => $v)
      	{
			if (!isset($array[$k])) return array();
			if (empty($result)) $result=array_keys($array[$k],$v);
			else
			{
				$uniquekeys=array_keys($array[$k],$v);
				$result=array_intersect($result,$uniquekeys);
			}
			if (empty($result)) return $result;
    	}
    	// Return the result array
    	return $result;
	}
	

	function bookObject() {
		$uids=$this->objectuids;
		$aUid=explode(',',$uids); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookObject: prevevent: '.$this->aParms['prevevent'].'; objecuids:'.$uids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
		if (!empty($this->aParms['prevevent'])){
			// check if same objects:
			$eventuid = $this->aParms['prevevent'];
			$data = $this->getEvent($eventuid);
			$objectuids=$data['objectuids'];
			if ($objectuids == $uids){
				if (empty($data['repeatperiod']) && $this->datecur->format('H') > $data["endhour"]){ // no recurrent event
					// update end time:
					$fields=array('endhour' => $this->datecur->format('H'));
					$where = "  uid = ".intval($eventuid);
					$result = $this->db->query('update tx_bookingpar_event set endhour = "'.$this->datecur->format('H').'" where '.$where );
				}
			}
			else {
				$this->aParms['prevevent']='';  // new event, reset previous event
			}
		}
		else
		{
			// create event:
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookObject: objecten '.$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												

			$fields = array (
			'pid' 		=> $this->conf['pid_list'],
			'objectuids' => $this->objectuids,
			'feuseruid' => $this->feuserid,
			'startdate' => $this->datecur->getTimestamp(),
			'enddate' 	=> $this->datecur->getTimestamp(),
			'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
			'cruser_id' => $this->feuserid,
			'crdate' 	=> time(),
			'tstamp' 	=> time(),
			'starthour' => $this->datecur->format('H'),
			'endhour'	=> $this->datecur->format('H')
			);
			$stmnt= 'insert into `tx_bookingpar_event` (`'.implode("`, `",array_keys($fields))."`) values ('".implode("', '",array_values($fields))."' )";
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookObject: '.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
			$result = $this->db->query($stmnt);
			//if (!$result) error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookObject error: '.$db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/debug/debug.txt');		;	
			$eventuid= $this->db->insert_id;
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking aUids : '.urldecode(http_build_query($aUid,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (is_array($aUid))
		{ 
			$maxend=$this->datecur->getTimestamp();
			for ($i=0; $i<count($aUid);$i++){
		   		$fields = array (
				'pid' 		=> $this->conf['pid_list'],
				'objectuid' => $aUid[$i],
				'feuseruid' => $this->feuserid,
				'eventuid'	=> $eventuid,
				'startdate' => $this->datecur->getTimestamp(),
				'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
				'cruser_id' => $this->feuserid,
				'crdate' 	=> time(),
				'tstamp' 	=> time(),
				);
				$stmnt= 'insert ignore into `tx_bookingpar` (`'.implode("`, `",array_keys($fields))."`) values ('".implode("', '",array_values($fields))."' )";
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bookObject: insert bookings '.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												

				$result = $this->db->query($stmnt);
			}
		}
		$this->viewBookingForm();
	}

	function participate() {
	// participate to a event for a certain multibooking object and hour
		// check time and feUserUid
   		$fields = array (
		'pid' 		=> $this->conf['pid_list'],
		'objectuid' => $this->objectuids,
		'feuseruid' => $this->feuserid,
		'eventuid'	=> $this->aParms['event'],
		'startdate' => $this->datecur->getTimestamp(),
		'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
		'cruser_id' => $this->feuserid,
		'crdate' 	=> time(),
		'tstamp' 	=> time(),
		'sorting'	=> "1",
		);
		$stmnt= 'insert ignore into `tx_bookingpar` (`'.implode("`, `",array_keys($fields))."`) values ('".implode("', '",array_values($fields))."' )";
		$result = $this->db->query($stmnt);

		$this->viewBookingForm();
	}
	
	/**
 * Returns the rendered fluid email template
 * @param string $template
 * @param array $assign
 * @param string $ressourcePath
 * @return string
 */
/*public function renderFluidTemplate($template, Array $assign = array(), $ressourcePath = NULL) {
	$rootPath=($ressourcePath === NULL ? 'EXT:bookingpar/Resources/Private/' : $ressourcePath);
    $ressourcesPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($rootPath);
	$templatePath=$ressourcesPath . 'Templates/Booking/' . $template;
    /* @var $view \TYPO3\CMS\Fluid\View\StandaloneView */
/*    $view = GeneralUtility::makeInstance(Standaloneview::class);
   	$view->getRequest()->setControllerExtensionName('bookingpar');
    $view->setTemplatePathAndFilename($templatePath);
	$layoutPath=$ressourcesPath . 'Layouts/';
	$layoutPaths=array($layoutPath);
    $view->setLayoutRootPaths($layoutPaths);
    $view->assignMultiple($assign);

    return $view->render();
} */
	
	
	function vieweditBooking() {
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking start '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
		// the template file
		$eventuid=$this->aParms['event'];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking event: '.$eventuid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking  userid: '.$this->feuserid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking  datecur: '.$this->datecur->format('Y-m-d H:i:s')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												

		$data=$this->getEvent($eventuid);
		$this->objectuids=$data["objectuids"];
		$listobjects= $this->getSelectObjects();
		$aObj=explode(',',$this->objectuids);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking objectuids : '.$this->objectuids.' aObj: '.urldecode(http_build_query($aObj,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking listobjects org: name '.$listobjects[4]->getName().', uid '.$listobjects[4]->getUid().' selected '.$listobjects[4]->getSelected()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		
		for ($i=0,$size=count($listobjects); $i<$size; ++$i)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking listobjects['.$i.'] name '.$listobjects[$i]->getName().', uid '.$listobjects[$i]->getUid().', selected '.$listobjects[$i]->getSelected()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			if (in_array($listobjects[$i]->getUid(),$aObj))
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."vieweditBooking listobjects $i selected "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				$listobjects[$i]->setSelected('selected="selected"'); 
			}
		}

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking listobjects processed: name '.$listobjects[4]->getName().', uid '.$listobjects[4]->getUid().', selected '.$listobjects[4]->getSelected()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$data["objectuids"]=$aObj;
		// min and max hours:
		$objects=$this->getObjects();
		$aHours=$objects['hours'];
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking aHours : '.urldecode(http_build_query($aHours,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$hourmin=23;
		$hourmax=0;
		foreach ($aHours as $Hour)
		{
			$aHour=explode(',',$Hour);
			$hourmin=min(min($aHour),$hourmin);
			$hourmax=max(max($aHour),$hourmax);
		}
		// Month pattern:
		if (empty($data["monthsequencenbr"]))
			$monthsequencenbr=(string)min(4,floor((date('d',$data['startdate'])+6)/7));
		else $monthsequencenbr=$data["monthsequencenbr"];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking monthsequencenbr : '.$monthsequencenbr."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$aMonth=[0,0,0,0];
		for ($i=1;$i<5;$i++) if (strpos($monthsequencenbr,strval($i))!==false) $aMonth[$i-1]=1;
		$data['monthsequencenbr']=$aMonth;
		
		if (empty($data['weekdays']))$data['weekdays']=date('N',$data["startdate"]); // day of week from current booking:
		$weekdays=$data['weekdays'];
		$aWeekdays=[0,0,0,0,0,0,0];
		for ($i=1;$i<8;$i++) if (strpos($weekdays,strval($i))!==false) $aWeekdays[$i-1]=1;
		$data['weekdays']=$aWeekdays;
		
        $days = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$start=new \DateTime("Monday");
		for ($i=0;$i<7;$i++)
		{
			$day= new Day();
			$day->setUid(strftime("%u",$start->getTimestamp()));
			$day->setName(strftime("%A",$start->getTimestamp()));
			$start->modify("+ 1 day");
			$days->attach($day);
		}
		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'vieweditBooking objectuids: '.$this->objectuids."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');												
		$datemin=new \DateTime();
		$datmin=$datemin->format("Y-m-d");
		$datmax=$this->datemax->format("Y-m-d");
    	$assignedValues = [
			'event' => $data,
			'conf' => $this->conf,
			'selectoption'=>$listobjects,
			'disabled'=> (($this->beheer)?'':'disabled'),
 			'datemax'=> $datmax,
			'datemin'=> $datmin,
			'hourmin'=> $hourmin,
			'hourmax'=> $hourmax,
			'days' => $days,
			'useruid' => $this->feuserid,
			'lang' => $this->conf['lang'],
             ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingEid event : '.urldecode(http_build_query($data,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$out= $this->renderFluidTemplate("VieweditBooking.html", $assignedValues) ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BookingEid out : '.$out."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//		$this->antwoord .=   $out;
		$this->antwoord= FluidTemplate::render("Booking/VieweditBooking.html", $assignedValues,$this,'bookingpar','churchpersreg') ;
	}

	function saveEvent() {
	//	list($day, $month,$year) = explode('-', $this->conf['date']);
	//	$this->aParms['day'] = $year . '-' . $month . '-' . $day ;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent aParms : '.urldecode(http_build_query($this->aParms,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$eventuid=$this->aParms['event']; 
		$errormsg='';
		$idorganisernw='';
		$objectuidsnw=$this->aParms['objectsselectednw'];
		$data=$this->getEvent($eventuid);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent data: '.http_build_query($data,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$objectuids=$data["objectuids"];
		$starthournw=(int)$this->aParms['starthour'];
		$starthour=(int)$data["starthour"];
		$startdatenw = new \DateTime($this->aParms['startdate'].' '.$this->aParms['starthour'].':00:00');
//		$startdatenw = new \DateTime(strval($this->aParms['startdate'])); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent startdatenw: '.$startdatenw->getTimestamp().'; startdate:'.strval($this->aParms['startdate']).'; strtotime:'.strtotime($this->aParms['startdate']).'; inverse:'.$startdatenw->format('Y-m-d')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$startdate=new \DateTime();
		$startdate->setTimestamp($data["startdate"]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent startdate: '.$startdate->format("Y-m-d").', startdatenw:'.$startdatenw->format("Y-m-d")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$enddate=new \DateTime();
		$enddate->setTimestamp($data["enddate"]);
		$enddate->setTimezone(new \DateTimeZone("UTC"));
		$enddatenw=new \DateTime(strval($this->aParms['enddate'])); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent enddate: '.$enddate->format("Y-m-d").', enddatenw:'.$enddatenw->format("Y-m-d")."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		$TimeOffSet=$startdate->getOffset()/3600; 
/*		$starthourUTC= new \DateTime($data["startdate"]." ".$starthournw.":00:00");
			$starthourUTC->setTimezone(new \DateTimeZone("UTC"));
			$srthrnw=$starthourUTC->format("H");
*/

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent startdate: '.$startdate->getTimestamp().', TimeOffSet:'.$TimeOffSet."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent startdatenw: '.$startdatenw->getTimestamp().', TimeOffSet:'.$TimeOffSet."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent enddate: '.$enddate->getTimestamp()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$enddatenw=new \DateTime(strval($this->aParms['enddate'])); 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent enddatenw: '.$enddatenw->getTimestamp()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$startdatenw=strtotime($startdatenw);
		$endhournw=(int)$this->aParms['endhour'];
		$endhour=(int)$data["endhour"];
		$repeatperiodnw=$this->aParms['repeatperiod'];
		$repeatperiod=$data['repeatperiod'];
		$repeatintervalnw=$this->aParms['repeatinterval'];
		$repeatinterval=$data['repeatinterval'];
		$weekdays=$data['weekdays'];
		$weekdaysnw=$this->aParms['weekdays'];
		$memo=$data['memo'];
		$memonw=$this->aParms['memo'];
		$monthsequencenbr=$data['monthsequencenbr'];
		$monthsequencenbrnw=$this->aParms['monthsequencenbr'];
		$idorganiser=(int)$data["organiser"];
		$idorganisernw=(int)$this->aParms['idorganiser'];
		if (empty($repeatperiodnw))$enddatenw=$startdatenw; // no repeating: enddate = startdate
		// check if anything has been changed:
		if ($objectuidsnw == $objectuids && $starthournw==$starthour && $endhournw==$endhour && $startdatenw==$startdate 
		&& $repeatperiodnw==$repeatperiod && $repeatintervalnw==$repeatinterval && $weekdaysnw==$weekdays
		&& $monthsequencenbrnw==$monthsequencenbr && $enddatenw==$enddate
		&& (string)$this->aParms['memo']===(string)$data["memo"]
		&& $idorganisernw===$idorganiser)
		{
			// nothing changed:
			header("SAVE_SUCCESS: 2"); 
			return;
		}
		// check valid data:
/*		$now=strtotime("now");
		$now=date("d-m-Y",$now);
		$now=strtotime($now); */
		$now=new \DateTime();
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent startdatenw: '.$startdatenw->getTimestamp().'; startdate: '.$startdate->getTimestamp().'; datemax: '.$this->datemax->getTimestamp()."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		
		if ($startdatenw!=$startdate){
			if ($startdatenw->format('Y-m-d')!=$this->aParms['startdate'])
				$errormsg=$this->getLL("illegaldate").": ".$this->getLL('startdate').' '.strftime('%x',$startdatenw->getTimestamp());
			elseif ($startdatenw< $now)
				$errormsg=$this->getLL("datepast").": ".strftime('%x',$startdatenw->getTimestamp());
			elseif ($startdatenw>$this->datemax)
				$errormsg=$this->getLL("datefuture").": ".strftime('%x',$startdatenw->getTimestamp());
		}
		if (empty($errormsg) && !empty($repeatperiodnw) && $enddatenw!=$enddate){
			if ($enddatenw->format('Y-m-d')!=$this->aParms['enddate'])
				$errormsg=$this->getLL("illegaldate").": ".$this->getLL('enddate').' '.strftime('%x',$enddatenw->getTimestamp());
			elseif ($enddatenw< $now)
				$errormsg=$this->getLL("datepast").": ".strftime('%x',$enddatenw->getTimestamp());
			elseif ($enddatenw>$this->datemax)
				$errormsg=$this->getLL("datefuture").": ".strftime('%x',$enddatenw->getTimestamp());
			elseif ($enddatenw<$startdatenw)
				$errormsg=$this->getLL("enddateafterstart").": ".strftime('%x',$enddatenw->getTimestamp());
		}
		if (empty($errormsg) && !empty($repeatperiodnw)){
			if ($repeatperiodnw=='W' && empty($weekdaysnw))
				$errormsg=$this->getLL("weekdayselect");
			elseif ($repeatperiodnw=='M' && empty($monthsequencenbrnw))
				$errormsg=$this->getLL("monthsequencenbrnwselect");
		}
		if (empty($errormsg) && empty($idorganisernw)) $errormsg=$this->getLL("personselect");
		if (empty($errormsg)){	
			$aDates=$this->generateDates($startdatenw,$enddatenw,$repeatintervalnw,$repeatperiodnw,$weekdaysnw,$monthsequencenbrnw);
			if (empty($aDates))$errormsg="geen data";
			else {
				$objects = $this->getObjects($objectuidsnw);
				$aHoursR=$this->getReservableHours($aDates["dayofweek"],$objects);
				// check if changed event fits within reservable hours per day:
				if (min($aHoursR)>$starthournw || max($aHoursR)<$endhournw) 
				{	$errormsg= $this->getLL("bookingoutsidehours").': '.min($aHoursR).' - '.max($aHoursR);
				}
			}
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check valid data errormsg: '.$errormsg.', starthournw:'.$starthournw.', endhournw:'.$endhournw."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		if (empty($errormsg)) {
			// check if all corresponding bookings do not conflict with current bookings of other events:
			// select user,objectname,startdate from bookings with eventuid!=myeventuid and startdate in (generated dates) and objectid in (objectuidsnw):
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent aDates : '.urldecode(http_build_query($aDates,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

			$starthourUTC= new \DateTime($aDates["date"][0]." ".$starthournw.":00:00");
			$starthourUTC->setTimezone(new \DateTimeZone("UTC"));
			$srthrnw=$starthourUTC->format("H");
			$endhourUTC= new \DateTime($aDates["date"][0]." ".$endhournw.":00:00");
			$endhourUTC->setTimezone(new \DateTimeZone("UTC"));
			$endhrnw=$endhourUTC->format("H");
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check bookings endhrnw: '.$endhrnw."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			
			$fields = "a.startdate,a.memo,concat(o.number,' ',o.name) as objectname, concat_ws(' ',roepnaam,tussenvoegsel,achternaam) as bookername ";
			$where = "  a.pid IN (".$this->conf['pid_list'].") AND a.deleted='0' AND a.hidden='0' AND b.deleted='0' AND b.disable='0'";
			$where .= "  AND objectuid in (" . $objectuidsnw . ")";
			$where .= ' AND from_unixtime(a.startdate,"%Y-%m-%d") in ("'.implode('", "',$aDates["date"]).'") '.
			"and from_unixtime(a.startdate,'%H') between ".$srthrnw." and ".$endhrnw;
			$where .= " AND b.uid = a.feuseruid AND a.objectuid = o.uid AND p.uid=b.person_id";
			$where .= " AND a.eventuid != ".$eventuid;
			$this->aParms['orderBy'] = 'a.startdate,o.number';
			$query='select '.$fields.' from tx_bookingpar a, fe_users b, tx_bookingpar_object o, persoon p where '.$where.' order by '.$this->aParms['orderBy'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check bookings query: '.$query."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$result = $this->db->query($query );
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check bookings error: '.$this->db->error.'; aantal rijen:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			if ($result->num_rows>0)
			{
				$occupied=$this->getLL("occupied");
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent occupied vertaald: '.$occupied."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
				$errormsg=$this->getLL("occupied");
				while ($row = $result->fetch_array(MYSQLI_ASSOC))
				{	
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent occupied: '.$row['objectname'].' booker:'.$row['bookername'].', memo:'.$row['memo']."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	
					$errormsg.="<br>";
					$errormsg.=date("d-m-Y H:i",$row['startdate']).'  '.$row['objectname'].'  '.$row['bookername'].'  '.$row['memo'];
				}
			} 
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent bookings errormsg: '.$errormsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	
		if (empty($errormsg))
		{
			if(!empty($idorganisernw))$user=$idorganisernw;
			//$newrepeatadded=0;
			//$datemoved='';
			//$timeblockmoved=0;
			$objectschanged=0;
			//$newrepeatadded = (empty($repeatperiod) && !empty($repeatperiodnw) && $startdate->format("Y-m-d") == $aDates["date"][0] && $objectuidsnw == $objectuids);
			//$datemoved = ($startdate == $enddate && $startdatenw == $enddatenw && $startdatenw!=$startdate);
			//$timeblockmoved = ($starthournw > $endhour || $endhournw < $starthour);
			$objectschanged = ($objectuidsnw != $objectuids );
//			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent save new dates newrepeatadded: '.$newrepeatadded.'; datemoved:'.$datemoved.'; timeblockmoved:'.$timeblockmoved.'; objectschanged:'.$objectschanged."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');				
			//$newendhour=$endhournw-$TimeOffSet;
			$datetimechanged=($startdatenw->format('Y-m-d')!=$startdate->format('Y-m-d') || $enddatenw->format('Y-m-d')!=$enddate->format('Y-m-d') || $starthournw!=$starthour || $endhournw!=$endhour);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent save new dates repeatperiodnw: '.$repeatperiodnw.'; repeatperiod:'.$repeatperiod.'; repeatintervalnw:'.$repeatintervalnw.'; repeatinterval:'.$repeatinterval."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			
//			$repeatchanged = ( $repeatperiodnw!=$repeatperiod || ($repeatintervalnw!=$repeatinterval || $weekdaysnw!=$weekdays || $monthsequencenbrnw!=$monthsequencenbr) && !empty($repeatperiodnw) );
			$repeatchanged = ( $repeatperiodnw!=$repeatperiod || ($repeatintervalnw!=$repeatinterval || $weekdaysnw!=$weekdays || $monthsequencenbrnw!=$monthsequencenbr)  );
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent save new dates repeatchanged: '.$repeatchanged."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');				$newendhour=$endhournw-$TimeOffSet;
			//$removeinsert=(!$newrepeatadded && ($datemoved || $timeblockmoved || $objectschanged || $repeatchanged ));
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent objectschanged: '.$objectschanged.', datetimechanged: '.$datetimechanged.', repeatchanged: '.$repeatchanged."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');				$newendhour=$endhournw-$TimeOffSet;
//			if (!$newrepeatadded && ($datemoved || $timeblockmoved || $objectschanged || $repeatchanged) ;
//			if ( !$newrepeatadded && ($datemoved || $timeblockmoved || $objectschanged || $repeatchanged) )
			if ( $datetimechanged || $objectschanged || $repeatchanged )
			{
				// remove all bookings and insert new bookings
				// remove current bookings of the event and generate the new ones:
				$where = 'eventuid=' . $eventuid;
				$stmnt='delete from `tx_bookingpar` where '.$where;
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent remove all bookings stmnt:'.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');				$newendhour=$endhournw-$TimeOffSet;
				$result = $this->db->query('delete from `tx_bookingpar` where '.$where);
				// add new bookings for each date, hour and object:
				$aObjects=explode(",",$objectuidsnw);
				if (!empty($aObjects))
				{
					for ($obj=0; $obj<count($aObjects);$obj++){
						foreach ($aDates['date'] as $date)
						{
							for ($hour=$starthournw; $hour <=$endhournw; $hour++)
							{
								$start = \DateTime::createFromFormat("Y-m-d H",$date." ".$hour);
	
						   		$fields = array (
								'pid' 		=> $this->conf['pid_list'],
								'objectuid' => $aObjects[$obj],
								'feuseruid' => $user,
								'eventuid'	=> $eventuid,
								'startdate' => $start->getTimestamp(),
								'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
								'cruser_id' => $this->feuserid,
								'crdate' 	=> time(),
								'tstamp' 	=> time()
								);
								$stmnt= 'insert into `tx_bookingpar` (`'.implode("`, `",array_keys($fields))."`) values ('".implode("', '",array_values($fields))."' )";
								//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent insert new bookings stmnt:'.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');				$newendhour=$endhournw-$TimeOffSet;
								$result = $this->db->query($stmnt);
							}
						}
					}
				}
			}
			else
			{
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent change memo of organiser: '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

				// change memo:
				if ($memonw!=$memo)
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent change memo:'.$memonw."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
					$newendhour=$endhournw-$TimeOffSet;
					// change remaining dates with new memo:
					$fields = array (
					'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
					'tstamp' 	=> time(),
					);
					$where = "  deleted=0 and eventuid = ".$eventuid;
//					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveevent nw memo per boeking: '."update `tx_bookingpar` set `memo`= '".strip_tags($this->aParms['memo'])."', `tstamp` = ".time().' where '.$where ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
									
					$result = $this->db->query("update `tx_bookingpar` set `memo`= '".$this->db->escape_string(strip_tags($this->aParms['memo']))."', `tstamp` = ".time().' where '.$where );
				}
				// change organiser:

				if (!empty($idorganiser) && $idorganisernw!==$idorganiser )
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent change organiser: '.$idorganisernw."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
					// change remaining dates with new organiser:
					$fields = array (
					'feuseruid' 		=> $idorganisernw,
					'tstamp' 	=> time(),
					);
					$where = "  deleted=0 and eventuid = ".$eventuid." and  feuseruid=".$idorganiser;
					$statement="update `tx_bookingpar` set `feuseruid`= ".$idorganisernw.', `tstamp` = '.time().' where '.$where;
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent change organiser: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

					$result = $this->db->query($statement );
					//if (!$result) error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveevent stmnt:'.$statement.'; error: '.$db->error.'; aantal rijen:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
				}
			}
		}
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check valid fields before save errormsg: '.$errormsg.'; error: '.$this->db->error.'; aantal rijen:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent check valid fields before save errormsg: '.(string)$errormsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		if (empty($errormsg)) 
		{
			// change event:
		//	$this->objectuids=$objectuidsnw;
			$fields = array (
			'memo' 		=> $this->db->escape_string(strip_tags($this->aParms['memo'])),
			'tstamp' 	=> time(),
			'objectuids' => $objectuidsnw,
			'startdate'	=> $startdatenw->getTimestamp(),
			'starthour'	=> $starthournw,
			'endhour'	=> $endhournw,
			'repeatperiod'	=> $repeatperiodnw,
			'repeatinterval'	=> $repeatintervalnw,
			'weekdays'	=> $weekdaysnw,
			'monthsequencenbr'	=> $monthsequencenbrnw,
			'enddate'	=> $enddatenw->getTimestamp(),
			'feuseruid' => $idorganisernw
			);
			$where = " deleted=0 and uid = ".intval($eventuid);
			$fieldsstr= implode(', ', array_map(
        		function ($v, $k) { return sprintf("`%s`='%s'", $k, $v); },
        		$fields,
		        array_keys($fields)
		    ));
			$stmnt=	"update `tx_bookingpar_event` set ".$fieldsstr.' where '.$where;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveevent '.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');		
			$result = $this->db->query($stmnt);
		
			header("SAVE_SUCCESS: 1"); 
			$this->viewBookingForm();
		}
		else {
			header("SAVE_SUCCESS: 0"); 
			$this->antwoord .=  $errormsg;
		}
	}
	
	function cancelEvent() {
		header("SAVE_SUCCESS: 2");
	}	

	function deleteEvent() {
	//	list($day, $month,$year) = explode('-', $this->conf['date']);
	//	$this->aParms['day'] = $year . '-' . $month . '-' . $day ;
		$eventuid=$this->aParms['event'];
		$where = 'eventuid=' . $eventuid;
/*		$result = $GLOBALS['TYPO3_DB']->exec_DELETEquery(
											'tx_bookingpar',
											$where
											); */
		$result = $this->db->query('delete from `tx_bookingpar` where '.$where);
											
		// delete event when not used anymore:
		$result = $this->db->query('delete from `tx_bookingpar_event`where `uid`= '.$eventuid);
		
		header("SAVE_SUCCESS: 1"); 
		$this->viewBookingForm();
}
	
	function deleteBooking() {
		$eventuid=intval($this->aParms['event']);
		$result= $this->db->query('select objectuid,b.startdate,multibooking,e.feuseruid as organiser from tx_bookingpar b, tx_bookingpar_object o, tx_bookingpar_event e where b.uid='.$this->aParms['bookinguid'].' and o.uid=b.objectuid and e.uid=b.eventuid');
		$row=$result->fetch_assoc();
		if ($row['multibooking'] and $row['organiser']==$this->feuserid )
			$where="eventuid=".$eventuid." and objectuid=".$row['objectuid']." and startdate=".$row['startdate']; // delete also corresponding bookings of participants
		else
			$where = 'uid='.$this->aParms['bookinguid'];   // delete only this booking
		$statement='delete from `tx_bookingpar`where '.$where;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
		$this->db->query($statement);
		// check if last occurrence of starthour removed:
		$data=$this->getEvent($eventuid);
		if (empty($data["repeatperiod"])){
			// no recurrent event, so change event:
			$where = 'eventuid=' . $eventuid;
			$orderby = 'startdate desc';
			$statement='select `uid`,`startdate` from `tx_bookingpar` where '.$where.' order by '.$orderby.' limit 1';
			$result = $this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking check if last occurrence of starthour removed statement:'.$statement.';numrows: '.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
			if ($result->num_rows>0)
			{
				$row = $result->fetch_array(MYSQLI_ASSOC);
				$endhournew = date('H',$row['startdate']);
				$orderby = 'startdate asc';
				$statement='select `uid`,`startdate` from `tx_bookingpar` where '.$where.' order by '.$orderby.' limit 1';
				$result = $this->db->query($statement);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking get startdate:'.$statement.';numrows: '.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
				if ($result->num_rows>0)
				{
					$row = $result->fetch_array(MYSQLI_ASSOC);
					
					$starthournew = date('H',$row['startdate']);
					$starthourcur = $data["starthour"];
					$endhourcur = $data["endhour"];
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking starthournew:'.$starthournew.';starthourcur: '.$starthourcur.'; endhourcur : '.$endhourcur.'; endhournew : '.$endhournew."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
					if ($starthournew!=$starthourcur || $endhournew != $endhourcur)
					{	// update event:
						//$fields=array('endhour' => $endhournew,'starthour' => $starthournew);
						$where = "  uid = ".$eventuid;
	/*					$result = $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_bookingpar_event',
											$where,
											$fields); */
						$statement='update `tx_bookingpar_event` set endhour = "'.$endhournew.'",starthour = "'. $starthournew.'" where '.$where; 
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking update event:'.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
						$result = $this->db->query($statement );
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking update event affected rows:'.$this->db->affected_rows.'; error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
					}
				}
			}
		}

		// delete event when not used anymore:
/*		$result = $GLOBALS['TYPO3_DB']->exec_DELETEquery(
											'tx_bookingpar_event',
											'uid='.$this->conf['eventUid'].' and uid not in (select eventuid from tx_bookingpar where eventuid='.$this->conf['eventUid'].' and pid='.$this->conf['pid_list'].')'
											); */
		$stmnt='delete from `tx_bookingpar_event` where uid='.intval($eventuid).' and uid not in (select eventuid from tx_bookingpar where eventuid='.intval($eventuid).')';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking delete event stmnt:'.$stmnt."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
		$result = $this->db->query($stmnt);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'deleteBooking delete event affected rows:'.$this->db->affected_rows.'; error: '.$this->db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');	
		$this->viewBookingForm();
	}

	function getReservableHours($aDates,$aobjectuids){
		// determine the intersection of all resvervable hours on all dates and for all objects
		$hours="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23";
		$aHours=explode(",",$hours);
		foreach ($aDates as $adat){
			if ($adat==7){
				foreach ($aobjectuids['sundayhours'] as $aobj){
					$aHours2 = explode(',', $aobj);
					$aHours = array_intersect($aHours,$aHours2);
				}
			}
			else {
				foreach ($aobjectuids['hours'] as $aobj){
					$aHours2 = explode(',', $aobj);
					$aHours = array_intersect($aHours,$aHours2);
				}
			}
		}
		return array_values($aHours);
	}
	
	function generateDates($startdate,$enddate,$repeatinterval,$repeatperiod,$weekdays,$monthsequencenbr){
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent generateDates enddate: '.$enddate->format("Y-m-d").'; repeatinterval:'.$repeatinterval.'; repeatperiod:'.$repeatperiod.'; weekdays:'.$weekdays."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		// determine dates corresponding with the event;
		$aDates=array();
		if (empty($repeatperiod)){
			$aDates["date"][0]=$startdate->format("Y-m-d");
			$aDates["dayofweek"][0]=$startdate->format("N");  //7=Sunday
		}
		if ($repeatperiod=="W"){
	//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent generate date voor period week till enddate: '.$enddate->format("Y-m-d").'; error: '.$this->db->error.'; aantal rijen:'.$result->num_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

			// generate dates for recurrent meeting:
			$enddatecur = clone $enddate;
			//$enddatecur->setTimestamp($enddate);
			$enddatecur->setTime(0,0);

			$datebegin  = clone $startdate;
			//$datebegin->setTimestamp($startdate);
			$datebegin->setTime(0,0);
			$startdatecur= clone $datebegin;
			$weekdaycur=$startdate->format("N"); 
			$interval = new \DateInterval('P'.($weekdaycur-1).'D');
			$datebegin->sub($interval);
			$adofw=str_split($weekdays);
			$dt=0;
			$step=new \DateInterval('P'.($repeatinterval*7).'D');
			for ($datebegin;$datebegin<=$enddatecur; $datebegin->add($step)){
				foreach ($adofw as $weekday){
					$bookdate = clone $datebegin;
					$bookdate->add(new \DateInterval('P'.($weekday - 1).'D'));
					if ($bookdate>=$startdatecur && $bookdate <=$enddatecur)
					{
						$aDates["date"][$dt]=$bookdate->format("Y-m-d");
						$aDates["dayofweek"][$dt]=$weekday;
						$dt++;
					}
				}
			}
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent repeatperiod Week aDates: '.urldecode(http_build_query($aDates,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		}
		if ($repeatperiod=="M"){
			$aMseq=str_split($monthsequencenbr);
			$starthour = $startdate->format('H');
			$curmonth= new \DateTime($startdate->format('Y-m-').'01');
			$weekdayreq=intval($weekdays);
			$dt=0;
			for ($curmonth; $curmonth<=$enddate; $curmonth->modify("+1 month")){
				$weekdaystart=intval($curmonth->format('N'));
				$nbrdays = $curmonth->format("t");
				//intval(date('N', strtotime("01-".date("m-Y",$curmonth))));
				foreach ($aMseq as $mseq){
					// determine date of n-th weekdays:
					$dayinmonth=(($weekdayreq - $weekdaystart+7) % 7)+1 + ($mseq-1)*7;
					if ($dayinmonth <= $nbrdays){
						$datereq= new \DateTime($curmonth->format('Y-m-').str_pad($dayinmonth,2,'0',STR_PAD_LEFT).' '.$starthour.':00:00');
						//$datereq=strtotime(str_pad($dayinmonth,2,'0',STR_PAD_LEFT).'-'.date("m-Y",$curmonth));
					 	if ($datereq<=$enddate && $datereq>=$startdate) {
							$aDates["date"][$dt]=$datereq->format("Y-m-d");
							$aDates["dayofweek"][$dt]=$weekdayreq;
							$dt++;
						}
					}
				}
			}
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveEvent repeatperiod Month aDates: '.urldecode(http_build_query($aDates,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		}
		return $aDates;
	}
	
	function getLL($s) {
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'getLL: '.$s."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		return $this->languageService->sL('LLL:EXT:bookingpar/Resources/Private/Language/locallang.xlf:'.$s); 

		//return $this->language->getLLL($s,$this->LOCAL_LANG);
	}
	

	function getListPageBrowser(&$marks, &$ajaxData) {
		// $nC = number of center pages
		// display like this 1 2 .. 4 5 6 7 8 .. 10 11
		// when actual page is 6
		$nc = 5;
		$p = $this->aParms['page'];
		if ($this->conf['listRecordsPerPage'] > 0 ) $n = ceil($this->counts / $this->conf['listRecordsPerPage']);
		$marks['###PAGEBROWSER###'] = '';
		if ($n == 1) return;
		// simple page browser
		if ($n <= $nc + 4) {
	        for ($i = 1; $i <= $n; $i++) {
				if ($i == $p)
					$marks['###PAGEBROWSER###'] .= '<span class="act">' . $i . '</span>';
				else
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['mode'] . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
	        }
		}
		// extended page browser
		if ($n > $nc + 4) {
		    if ($p <= (int)($nc / 2 ) + 2) {
		        // 1 2 3 4 5 6 7.. 13 14
		        for ($i = 1; $i <= $nc + 2; $i++) {
					if ($i == $p)
						$marks['###PAGEBROWSER###'] .= '<span class="act">' . $i . '</span>';
					else
						$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
		        }
				$marks['###PAGEBROWSER###'] .= '..';
		        for ($i = 1; $i >= 0; $i--) {
						$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 ($n - $i) . '\');">' . ($n - $i) . '</span>';
	        }
		    }
			if ($p + nc / 2 <= $n - 4 && $p > $nc / 2 + 2) {
			    // 1 2 .. 8 9 10 11 12 .. 15 16
		        for ($i = 1; $i <= 2; $i++) {
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
		        }

				if ($p - (int)($nc / 2) != 3)$marks['###PAGEBROWSER###'] .= '..';

		        for ($i = $p - (int)($nc / 2); $i <= $p + (int)($nc / 2); $i++) {
					if ($i == $p)
						$marks['###PAGEBROWSER###'] .= '<span class="act">' . $i . '</span>';
					else
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
		        }

				if ($p + (int)($nc / 2) != $n - 2) $marks['###PAGEBROWSER###'] .= '..';
		        for ($i = n - 1; $i <= n; $i++) {
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 ($i + $n) . '\');">' . ($i + $n) . '</span>';
		        }
			}
			if ($p + nc / 2 >= $n - 3) {
				// 1 2 .. 22 23 24 25 26
		        for ($i = 1; $i <= 2; $i++) {
					if ($i == $p)
						$marks['###PAGEBROWSER###'] .= '<span class="act">' . $i . '</span>';
					else
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
		        }
				$marks['###PAGEBROWSER###'] .= '..';
		        for ($i = $n - $nc; $i <= $n; $i++) {
					if ($i == $p)
						$marks['###PAGEBROWSER###'] .= '<span class="act">' . $i . '</span>';
					else
					$marks['###PAGEBROWSER###'] .= '<span class="norm" onclick="tx_booking_submit(\'' . $this->aParms['mode'] . '\',\''.
					 	$ajaxData . '\', \'' . $this->aParms['orderBy'] .'\', \'' . $this->aParms['sort'] . '\', \'' .
						 $i . '\');">' . $i . '</span>';
		        }
			}
		}
	}

	function HeeftPermissie($permissie_)
	{
		// ga na of aan ��n van de gevraagde permissies in $permissie_ is voldaan.
		$permissieNodigArray_ = explode(",",strtoupper($permissie_));
		$gevonden_=false;
		$permissie_array_=$this->permissie;
						
		foreach ($permissieNodigArray_ as $permissienodig)
		{	if (!empty($permissie_array_))
			{	foreach ($permissie_array_ as $permission)
				{	
					If (strcasecmp($permission,$permissienodig)==0 OR strcasecmp($permission,churchpersreg_div::ADMINFUNC)==0){$gevonden_=true;break 2;}
				}
			}
		}
		return $gevonden_;
	} 


}

