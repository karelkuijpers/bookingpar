<?php
namespace Parousia\Bookingpar\Domain\Model;

/***
 *
 * This file is part of the "bookingpar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class BookingHour extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
	 * uid of Hour
     * @var int
     */
    protected $uid = null;

    /**
     * hour
     *
     * @var string
     */
    protected $hour = null;

    /**
     * startdate
     *
     * @var /DateTime
     */
    protected $startdate = null;

    /**
     * username 
     *
     * @var string
     */
    protected $username = '';

    /**
     * roepnaam 
     *
     * @var string
     */
    protected $roepnaam = '';

    /**
     * achternaam 
     *
     * @var string
     */
    protected $achternaam = '';

    /**
     * objectname with tname of object
     *
     * @var string
     */
    protected $objectname = '';

    /**
     * seqnumber of object
     *
     * @var string
     */
    protected $seqnumber = '';

    /**
     * nameobject with nameobject of object
     *
     * @var string
     */
    protected $nameobject = '';

    /**
     * memo
     *
     * @var string
     */
    protected $memo = '';

    /**
     * eventuid with id of event 
     *
     * @var int
     */
    protected $eventuid = '';

    /**
     * boekinguid with id of booking 
     *
     * @var int
     */
    protected $bookinguid = '';

    /**
	 * editable 
     * @var int
     */
    protected $editable = 0;
	
    /**
	 * participatable 
     * @var int
     */
    protected $participatable = 0;
	
    /**
	 * bookable 
     * @var int
     */
    protected $bookable = 0;
	
    /**
	 * deletable 
     * @var int
     */
    protected $deletable = 0;
	


   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

   /**
     * Returns the hour
     *
     * @return string $hour
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Sets the hour
     *
     * @param string $hour
     * @return void
     */
    public function setHour($hour)
    {
        $this->hour = $hour;
    }

   /**
     * Returns the startdate
     *
     * @return /DateTime $startdate
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Sets the startdate
     *
     * @param /DateTime $startdate
     * @return void
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;
    }

   /**
     * Returns the editable property
     *
     * @return int $editable
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Sets the editable property
     *
     * @param int $editable
     * @return void
     */
    public function seteditable($editable)
    {
        $this->editable = $editable;
    }

   /**
     * Returns the participatable property
     *
     * @return int $participatable
     */
    public function getParticipatable()
    {
        return $this->participatable;
    }

    /**
     * Sets the participatable property
     *
     * @param int $participatable
     * @return void
     */
    public function setParticipatable($participatable)
    {
        $this->participatable = $participatable;
    }

   /**
     * Returns the bookable property
     *
     * @return int $bookable
     */
    public function getBookable()
    {
        return $this->bookable;
    }

    /**
     * Sets the bookable property
     *
     * @param int $bookable
     * @return void
     */
    public function setBookable($bookable)
    {
        $this->bookable = $bookable;
    }

   /**
     * Returns the deletable property
     *
     * @return int $deletable
     */
    public function getDeletable()
    {
        return $this->deletable;
    }

    /**
     * Sets the deletable property
     *
     * @param int $deletable
     * @return void
     */
    public function setDeletable($deletable)
    {
        $this->deletable = $deletable;
    }

   /**
     * Returns the username
     *
     * @return string $username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the username
     *
     * @param string $username
     * @return void
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

   /**
     * Returns the roepnaam
     *
     * @return string $roepnaam
     */
    public function getRoepnaam()
    {
        return $this->roepnaam;
    }

    /**
     * Sets the roepnaam
     *
     * @param string $roepnaam
     * @return void
     */
    public function setRoepnaam($roepnaam)
    {
        $this->roepnaam = $roepnaam;
    }

   /**
     * Returns the achternaam
     *
     * @return string $achternaam
     */
    public function getAchternaam()
    {
        return $this->achternaam;
    }

    /**
     * Sets the achternaam
     *
     * @param string $achternaam
     * @return void
     */
    public function setAchternaam($achternaam)
    {
        $this->achternaam = $achternaam;
    }

   /**
     * Returns the objectname
     *
     * @return string $objectname
     */
    public function getObjectname()
    {
        return $this->objectname;
    }

    /**
     * Sets the objectname
     *
     * @param string $objectname
     * @return void
     */
    public function setObjectname($objectname)
    {
        $this->objectname = $objectname;
    }

   /**
     * Returns the number
     *
     * @return string $number
     */
    public function getNumber()
    {
        return $this->seqnumber;
    }

    /**
     * Sets the number
     *
     * @param string $number
     * @return void
     */
    public function setNumber($number)
    {
        $this->seqnumber = $number;
    }

   /**
     * Returns the nameobject
     *
     * @return string $nameobject
     */
    public function getNameobject()
    {
        return $this->nameobject;
    }

    /**
     * Sets the nameobject
     *
     * @param string $nameobject
     * @return void
     */
    public function setNameobject($nameobject)
    {
        $this->nameobject = $nameobject;
    }

   /**
     * Returns the memo
     *
     * @return string $memo
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Sets the memo
     *
     * @param string $memo
     * @return void
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
    }

   /**
     * Returns the eventuid
     *
     * @return int $eventuid
     */
    public function getEventuid()
    {
        return $this->eventuid;
    }

    /**
     * Sets the eventuid
     *
     * @param int $eventuid
     * @return void
     */
    public function setEventuid($eventuid)
    {
        $this->eventuid = $eventuid;
    }


   /**
     * Returns the bookinguid
     *
     * @return int $bookinguid
     */
    public function getBookinguid()
    {
        return $this->bookinguid;
    }

    /**
     * Sets the bookinguid
     *
     * @param int $bookinguid
     * @return void
     */
    public function setbookinguid($bookinguid)
    {
        $this->bookinguid = $bookinguid;
    }

}
