<?php
namespace Parousia\Bookingpar\Domain\Model;

/***
 *
 * This file is part of the "bookingpar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class CalendarDay extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
	 * uid with day in month
     * @var int
     */
    protected $uid = null;

    /**
     * dateday with date of the day in format Y-m-d
     *
     * @var string
     */
    protected $dateday = null;

    /**
     * classday with classes of the day
     *
     * @var string
     */
    protected $classday = '';

    /**
     * title with titel for the day
     *
     * @var string
     */
    protected $title = '';

    /**
     * dayofweek
     *
     * @var int
     */
    protected $dayofweek = '';

    /**
     * week with number of week in the year
     *
     * @var int
     */
    protected $week = '';

    /**
	 * clickable for booking
     * @var int
     */
    protected $clickable = null;
	
   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
    */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

   /**
     * Returns the clickable property
     *
     * @return int $clickable
     */
    public function getClickable()
    {
        return $this->clickable;
    }

    /**
     * Sets the clickable property
     *
     * @param int $clickable
     * @return void
     */
    public function setClickable($clickable)
    {
        $this->clickable = $clickable;
    }

   /**
     * Returns the dateday
     *
     * @return string $dateday
     */
    public function getDateday()
    {
        return $this->dateday;
    }

    /**
     * Sets the dateday
     *
     * @param string $dateday
     * @return void
     */
    public function setDateday($dateday)
    {
        $this->dateday = $dateday;
    }

   /**
     * Returns the classday
     *
     * @return string $classday
     */
    public function getClassday()
    {
        return $this->classday;
    }

    /**
     * Sets the classday
     *
     * @param string $classday
     * @return void
     */
    public function setClassday($classday)
    {
        $this->classday = $classday;
    }

   /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

   /**
     * Returns the week
     *
     * @return int $week
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Sets the week
     *
     * @param int $week
     * @return void
     */
    public function setWeek($week)
    {
        $this->week = $week;
    }


   /**
     * Returns the dayofweek
     *
     * @return int $dayofweek
     */
    public function getDayofweek()
    {
        return $this->dayofweek;
    }

    /**
     * Sets the dayofweek
     *
     * @param int $dayofweek
     * @return void
     */
    public function setDayofweek($dayofweek)
    {
        $this->dayofweek = $dayofweek;
    }

}
