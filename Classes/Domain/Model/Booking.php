<?php
namespace Parousia\Bookingpar\Domain\Model;

/***
 *
 * This file is part of the "bookingpar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Booking extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var int
     */
    protected $uid = null;

    /**
     * the composed name of an object
     *
     * @var string
     */

    protected $name = '';
	
    /**
     * indicator if object is selected
     *
     * @var string
     */
    protected $selected = '';

	
	/**
	******  GETTERS & SETTERS
	**/
	
   /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
	
     /**
     * Returns the selected
     *
     * @return string $selected
     */
    public function getSelected()
    {
        return $this->selected;
    }
    /**
     * Sets the selected
     *
     * @param string $selected
     * @return void
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;
    }
	
   /**
     * Returns the uid
     *
     * @return int $uid
     */
    public function getUid():?int
    {
        return (int)$this->uid;
    }

    /**
     * Sets the uid
     *
     * @param int $uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
}
