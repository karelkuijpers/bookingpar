<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use Parousia\Bookingpar\Controller\BookingController;

defined('TYPO3') || die('Access denied.');

ExtensionUtility::configurePlugin(
         'Bookingpar',
         'Booking',
		[BookingController::Class => 'viewObjects,viewMonth'],
		[BookingController::Class => 'viewObjects,viewMonth']
    );

ExtensionUtility::configurePlugin(
         'Bookingpar',
         'Agenda',
		[BookingController::Class => 'viewAgenda'],
		[BookingController::Class => 'viewAgenda']
    );

// eID
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['tx_bookingpar_eID'] = \Parousia\Bookingpar\Hooks\tx_bookingpar_eID::class .'::processRequest';

?>