<?php
if (!defined ('TYPO3')) 	die ('Access denied.');
// allow booking_object and booking records on normal pages
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bookingpar_object');
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bookingpar');

