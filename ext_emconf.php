<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "bookingpar".
 *
 * Auto generated 18-05-2015 09:37
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Booking system',
	'description' => 'Booking system for reservation of meeting rooms',
	'category' => 'plugin',
	'author' => 'Karel Kuijpers',
	'author_email' => 'karelkuijpers@gmail.com',
	'shy' => 1,
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author_company' => '',
	'version' => '11.0.4',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.0-11.5.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:32:{s:9:"ChangeLog";s:4:"04f2";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"5b5d";s:14:"ext_tables.php";s:4:"0d5b";s:14:"ext_tables.sql";s:4:"c73d";s:28:"ext_typoscript_constants.txt";s:4:"da82";s:24:"ext_typoscript_setup.txt";s:4:"1961";s:15:"flexform_ds.xml";s:4:"1214";s:16:"locallang_db.xml";s:4:"9bb7";s:17:"locallang_tca.php";s:4:"5596";s:10:"README.txt";s:4:"ee2d";s:7:"tca.php";s:4:"8022";s:19:"doc/wizard_form.dat";s:4:"b48c";s:20:"doc/wizard_form.html";s:4:"43fc";s:31:"pi1/class.tx_bookingpar_eID.php";s:4:"9dfe";s:33:"pi1/class.tx_bookingpar_model.php";s:4:"7da9";s:31:"pi1/class.tx_bookingpar_pi1.php";s:4:"0874";s:14:"pi1/layout.css";s:4:"404d";s:17:"pi1/locallang.xml";s:4:"329e";s:17:"pi1/template.html";s:4:"a6fa";s:18:"pi1/doc/manual.sxw";s:4:"0ae5";s:21:"pi1/res/arrowleft.png";s:4:"d6c9";s:22:"pi1/res/arrowright.png";s:4:"a4e0";s:16:"pi1/res/book.gif";s:4:"4276";s:18:"pi1/res/delete.gif";s:4:"6de8";s:16:"pi1/res/edit.jpg";s:4:"456a";s:30:"pi1/res/wattstraat_beneden.jpg";s:4:"c944";s:28:"pi1/res/wattstraat_boven.jpg";s:4:"a8dd";s:19:"pi1/res/working.gif";s:4:"70d9";s:26:"pi1/scripts/multiselect.js";s:4:"53a6";s:24:"pi1/static/editorcfg.txt";s:4:"44c8";s:20:"pi1/static/setup.txt";s:4:"463f";}',
	'suggests' => array(
	),
);

?>
