#
# Table structure for table 'tx_bookingpar_object'
#
CREATE TABLE tx_bookingpar_object (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	hours varchar(70) DEFAULT '8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23' NOT NULL,
	sundayhours varchar(70) DEFAULT '12,13,14,15,16,17,18,19,20,21,22,23' NOT NULL,
	number char(5) DEFAULT '0.01' NOT NULL,
	multibooking tinyint(1) DEFAULT '0' NOT NULL,
	maxparticipants smallint(6) UNSIGNED DEFAULT '10000' NOT NULL ,
	monthsblocked varchar(30) DEFAULT '' NOT NULL,
	PRIMARY KEY (`uid`),
  	UNIQUE KEY `bookingobjectnumber` (`number`),
  	KEY parent (`pid`)
)
DEFAULT CHARACTER SET utf8 ;



#
# Table structure for table 'tx_bookingpar' - booking per object per hour
#
CREATE TABLE tx_bookingpar  (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	objectuid int(11) DEFAULT '0' NOT NULL,
	startdate int(11) DEFAULT '0' NOT NULL,
	enddate int(11) DEFAULT '0' NOT NULL,
	feuseruid int(11) DEFAULT '0' NOT NULL,
	memo varchar(255) DEFAULT ''  NOT NULL,
	eventuid int(11) DEFAULT '0' NOT NULL,
	PRIMARY KEY (`uid`),
	UNIQUE `enkeluur` (`eventuid`, `objectuid`, `startdate`, `feuseruid`),
	KEY parent (`pid`)
) DEFAULT CHARACTER SET utf8;

#
# Table structure for table 'tx_bookingpar_event'
#
CREATE TABLE tx_bookingpar_event (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	sorting int(10) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	startdate int(11) DEFAULT '0' NOT NULL,
	enddate int(11) DEFAULT '0' NOT NULL,
	feuseruid int(11) DEFAULT '0' NOT NULL,
	objectuids varchar(250) DEFAULT '' NOT NULL,
	starthour tinyint(2)  DEFAULT '0' NOT NULL,
	endhour tinyint(2)  DEFAULT '0' NOT NULL,
	memo varchar(512) DEFAULT ''  NOT NULL,
	repeatperiod char(1) DEFAULT ''  NOT NULL,
	repeatinterval tinyint(1) DEFAULT '0' NOT NULL,
	weekdays char(7) DEFAULT '' NOT NULL,
	monthsequencenbr char(4) DEFAULT '' NOT NULL,
	PRIMARY KEY (`uid`),
	KEY parent (`pid`)
)
DEFAULT CHARACTER SET utf8;

