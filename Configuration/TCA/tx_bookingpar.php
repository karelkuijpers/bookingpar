<?php
// ******************************************************************
// This is the standard TypoScript bookingpar table, tx_bookingpar
// ******************************************************************
return Array (
	"ctrl" => array (
		'title'     => 'LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar',
		'label'     => 'startdate',
		'label_alt'     => 'feuseruid',
		'label_alt_force'     => 'true',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'sortby' => 'sorting',
		'delete' => 'deleted',

		//'thumbnail' => 'image',
//		'selicon_field' => 'image',
//		'selicon_field_path' => 'fileadmin/onoffice',
		'enablecolumns' => array (
			'disabled' => 'hidden',
		),
		'iconfile' => 'EXT:bookingpar/Resources/Public/Icons/icon_tx_bookingpar.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "hidden, feuseruid",
	),
	"columns" => Array (
		"hidden" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),

        "objectuid" => Array (
			'suppress_icons' => '1',
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar.object",
			"config" => Array (
				"type" => "select",
				'renderType' => 'selectSingle',
				"items" => Array (
				    Array ('',0),
				),
				"foreign_table" => "tx_bookingpar_object",
				"foreign_table_where" => "and tx_bookingpar_object.pid=###CURRENT_PID### order by tx_bookingpar_object.name",
				"size" => "1",
				"minitems" => 1,
				"maxitems" => 1,
				"eval" => "trim",
				'wizards' => Array(
 					'_PADDING' => 1,
 					'_VERTICAL' => 1,
 					'edit' => Array(
 						'type' => 'popup',
 						'title' => 'Edit booking objects',
 						'module' => array('name' =>'wizard_edit.php'),
 						'icon' => 'actions-open',
						'popup_onlyOpenIfSelected' => 1,
 						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
 					'add' => Array(
 						'type' => 'script',
 						'title' => 'Create new booking object',
 						'icon' => 'actions-add',
 						'params' => Array(
 							'table'=>'tx_bookingpar_object',
 							'pid' => '###CURRENT_PID###',
 							'setValue' => 'prepend'
 						),
 						'module' => array('name' =>'wizard_add.php'),
 					),
					'list' => Array(
 						'type' => 'script',
 						'title' => 'List booking objects',
 						'icon' => 'actions-system-list-open',
 						'params' => Array(
 							'table'=>'bookingpar_object',
 							'pid' => '###CURRENT_PID###',
 						),
 						'module' => array('name' =>'wizard_list.php'),
					),
				),
			)
		),


		"startdate" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar.startdate",
			"config" => Array (
				"type" => "input",
				'renderType' => 'inputDateTime',
				"size" => "8",
				"eval" => "datetime",
				"checkbox" => "0",
				"default" => "0"
			)
		),
		"enddate" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar.enddate",
			"config" => Array (
				"type" => "input",
				'renderType' => 'inputDateTime',
				"size" => "8",
				"eval" => "datetime",
				"checkbox" => "0",
				"default" => "0"
			)
		),


		"feuseruid" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar.feuser",
			"config" => Array (
				"type" => "select",
				'renderType' => 'selectSingle',
				"items" => Array (
				    Array ('',0),
				),
				"foreign_table" => "fe_users",
				"foreign_table_where" => " order by fe_users.name",
				"size" => "1",
				"minitems" => 1,
				"maxitems" => 1,
				"eval" => "trim",
				'wizards' => Array(
 					'_PADDING' => 1,
 					'_VERTICAL' => 1,
					'list' => Array(
 						'type' => 'script',
 						'title' => 'List fe users',
 						'icon' => 'actions-system-list-open',
 						'params' => Array(
 							'table'=>'fe_users',
 							'pid' => '75',
 						),
 						'module' => array('name' =>'wizard_list.php'),
					),
				),
			)
		),


		'memo' => Array (
			'exclude' => 1,
			"label" => 'LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xml:tx_bookingpar.memo',
			'config' => Array (
				'type' => 'text',
				'cols' => '40',
				'rows' => '6',
				'wrap' => 'off'
			)
		),


	),
	"types" => Array (
		"0" => Array("showitem" => "objectuid, startdate, enddate, feuseruid, memo"),
	),

	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);
