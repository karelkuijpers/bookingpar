<?php
// ******************************************************************
// This is the standard TypoScript bookingpar table, tx_bookingpar_object
// ******************************************************************
return Array(
	"ctrl" => array(
		'title'     => 'LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object',
		'label'     => 'name',
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'sortby' => 'number',
		'delete' => 'deleted',
		'enablecolumns' => array (
			'disabled' => 'hidden',
		),
		'foreign_table_loadIcon' => '1',
		'iconfile' => 'EXT:bookingpar/Resources/Public/Icons/icon_tx_bookingpar_object.gif',
	),
	"feInterface" => array (
		"fe_admin_fieldList" => "hidden, name, hours,number",
	),
	"columns" => Array (
		"hidden" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"name" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.name",
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"eval" => "required,trim",
			)
		),
		"hours" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.hours",
			"config" => Array (
				"type" => "input",
				"size" => "40",
				"default" => "8,9,10,11,12,13,14,15,16,17,18,19,20,21,22",
				"eval" => "required,trim",
			)
		),
		"sundayhours" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.sundayhours",
			"config" => Array (
				"type" => "input",
				"size" => "40",
				"default" => "12,13,14,15,16,17,18,19,20,21,22",
				"eval" => "required,trim",
			)
		),
		"number" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.number",
			"config" => Array (
				"type" => "input",
				"size" => "5",
				"default" => "0.01",
				"eval" => "required,trim",
			)
		),
		"multibooking" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.multibooking",
			"config" => Array (
				"type" => "check",
				"default" => "0"
			)
		),
		"maxparticipants" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.maxparticipants",
			"config" => Array (
				"type" => "input",
				"size" => "40",
				"default" => "10000",
			)
		),
		"monthsblocked" => Array (
			"exclude" => 1,
			"label" => "LLL:EXT:bookingpar/Resources/Private/Language/locallang_db.xlf:tx_bookingpar_object.monthsblocked",
			"config" => Array (
				"type" => "input",
				"size" => "30",
				"default" => "",
				"eval" => "trim",
			)
		),

	),

	// 1-1-1 = style pointer which defines color, style and border
	"types" => Array (
		"0" => Array("showitem" => "name,hours,sundayhours,number,multibooking,maxparticipants,monthsblocked")
	),
	"palettes" => Array (
		"1" => Array("showitem" => "")
	)
);
