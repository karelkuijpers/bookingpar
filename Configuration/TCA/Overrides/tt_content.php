<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
defined('TYPO3') or die();

/***************
 * Plugin
 */
ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'list_type',
    'bookingpar',
    'Resereveren van ruimtes'
);

    $pluginSignature = ExtensionUtility::registerPlugin(
        'Bookingpar',
        'Booking',
        'Reservation of meeting rooms',
        'content-plugin-bookingpar-booking',
        'bookingpar',
    );
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
	ExtensionManagementUtility::addPiFlexFormValue(
	        $pluginSignature,
        	'FILE:EXT:bookingpar/Configuration/FlexForms/flexform_booking.xml'
	);

    $pluginSignature = ExtensionUtility::registerPlugin(
        'Bookingpar',
        'Agenda',
        'Overzicht reserveringen',
        'content-plugin-bookingpar-agenda',
        'bookingpar',
    );
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
	ExtensionManagementUtility::addPiFlexFormValue(
	        $pluginSignature,
        	'FILE:EXT:bookingpar/Configuration/FlexForms/flexform_booking.xml'
	);

/*
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['bookingpar_booking'] = 'recursive';

ExtensionManagementUtility::addToInsertRecords('tx_bookingpar_object');
ExtensionManagementUtility::addToInsertRecords('tx_bookingpar');
*/

